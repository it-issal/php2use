<?php

namespace PHP2USE\queue;

use PHP2USE\core\Observable;

use PHP2USE\web\Application;

/****************************************************************************/

class Task {
    private $alias;

    public function __construct ($alias=null) {
        if ($alias==null) {
            $alias = get_called_class();

            $alias = str_replace('\\', '.', $alias);

            if ($alias[0]=='.') {
                $alias = substr($alias, 1);
            }
        }

        $this->alias = $alias;

        $this->prepare();
    }

    /****************************************************************/

    public function alias  () { return $this->alias; }

    public function worker () { return Application::curr()->worker(); }
    public function queue  () { return Application::curr()->queue(); }

    /****************************************************************/

    public function execute ($job, $workload=null) {
        if ($job!=null) {
            if ($workload==null or sizeof($workload)==0) {
                $workload= $job->workload;
            }

            echo "Received job: " . $job->handle() . "\n";
            echo "Workload: $workload\n";
        }

        $result = $this->process($job, $workload);

        if ($job!=null) {
            for($i=1; $i<=10;  $i++) {
                $job->status($i,10);
                sleep(1);
            }

            echo "Result: $result\n";
        }

        return $result;
    }

    /****************************************************************/

    public function __invoke ($job, $extra=null) {
        return $this->execute($job);
    }

    /****************************************************************/

    public function invoke () {
        return $this->execute(null, func_get_args());
    }

    /****************************************************************/

    public function delay ($alias, $args=[]) {
        return $this->queue()->do($this->alias(), $args);
    }
}

