<?php

namespace PHP2USE\exceptions;

/****************************************************************************/

class ApiException extends \Exception {
    private $api = null;
    
    public function __construct ($api, $args = array()) {
        $this->api = $api;
        
        call_user_func_array(array($this, 'initialize'), $args);
    }
}

