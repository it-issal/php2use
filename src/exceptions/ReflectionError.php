<?php

namespace PHP2USE\exceptions;

/****************************************************************************/

class ReflectionException extends ApiException {
    public $verb;
    public $method;
    public $args;
    public $resp;
    
    protected function initialize ($verb, $method, $args, $resp) {
        $this->verb   = $verb;
        $this->method = $method;
        $this->args   = $args;
        $this->resp   = $resp;
    }
    
    public function render () {
        
    }
}

