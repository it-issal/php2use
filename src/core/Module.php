<?php

namespace PHP2USE\core;

/****************************************************************************/

class Module {
    public static function set ($key, $value) {
        Site::$cfg[$key] = $value;
    }
    public static function get ($key, $default=null) {
        if (Site::has($key)) {
            return Site::$cfg[$key];
        } else {
            return $default;
        }
    }
    public static function has ($key) {
        return array_key_exists($key, Site::$cfg);
    }
}

