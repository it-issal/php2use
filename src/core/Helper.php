<?php

namespace PHP2USE\core;

/****************************************************************************/

class Helper {
    protected function extract ($obj, $cb=null) {
        $resp = null;
        
        if ($cb!=null and $obj!=null) {
            try {
                $resp = $cb($obj);
            } catch (Exception $ex) {
                Site::handle_exc('Api', 'Flickr', $ex, array(
                    'resp' => $resp,
                ));
            }
        } else {
            $resp = $obj;
        }
        
        return $resp;
    }
    protected function wrap ($cls, $obj, $cb=null) {
        $ns = $this->rewrite_ns($cls);
        
        $obj = $this->extract($obj, $cb);
		
		if (is_object($obj) and $obj!=null) {
			$nrw = call_user_func(array($ns, 'narrow'), $obj);
			
			eval('$resp = new $ns($this, $nrw, $obj);');
			
			return $resp;
		} else {
			return null;
		}
    }
    protected function remap ($cls, $resp, $mpp=null, $cb=null) {
        $coll = array();
        
        if ($resp!=null) {
            $lst = $resp;
            
            if ($mpp!=null) {
                $lst = $mpp($resp);
            }
            
            foreach ($lst as $obj) {
                $coll[] = $this->wrap($cls, $obj, $cb);
            }
        }
        
        return $coll;
    }
}

