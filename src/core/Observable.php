<?php

namespace PHP2USE\core;

/****************************************************************************/

class Observable {
    protected function trigger ($obj, $method, $args=[]) {
        $target = null;

        switch ($obj) {
            case 'app':
                $target = $this->app;
                break;
            case 'this':
            case 'self':
            case null:
                $target = $this;
                break;
        }

        if ($target!=null) {
            return call_user_func_array(array($target, $method), $args);
        }

        return null;
    }

    protected static function reflect ($obj, $method, $args=[]) {
        $cls = get_called_class();

        $target = null; #$x = 1/0;

        switch ($obj) {
            case 'curr':
            case 'app':
                $target = $cls::$curr;
                break;
        }

        if ($target!=null) {
            return call_user_func_array(array($target, $method), $args);
        }

        return null;
    }

    /***********************************************************************/

    public static function rpath () {
        $cls = get_called_class();

        $args = func_get_args();

        if (sizeof($args)==1 and is_array($args[0])) {
            $args = $args[0];
        }

        $args = array_merge([$cls::bpath()], $args);

        return implode($args, '/');

        //return realpath(__DIR__.'/'.implode('/',func_get_args()));
    }
}

