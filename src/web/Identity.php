<?php

namespace PHP2USE\web;

use PHP2USE\core\Observable;

use PHP2USE\web\Application;

/****************************************************************************/

class Identity extends Observable {
    public static $RBAC = [
        'super' => [
            'title' => "Super Administrator",
        ],
        'dev' => [
            'title' => "Developer",
        ],
        'ops' => [
            'title' => "IT Ops",
        ],
    ];

    private $request;
    private $menus;

    public function __construct ($req) {
        $this->request = $req;

        $this->menus   = [];
    }

    /***********************************************************************/

    public function pseudo   () { return $_SESSION['profile']['user']; }
    public function fullname () { return $_SESSION['profile']['full']; }
    public function roles () {
        $resp = [];
        
        foreach ($_SESSION['profile']['role'] as $role) {
            $entry = Identity::$RBAC[$role];

            $entry['name'] = $role;

            $resp[] = $entry;
        }
        
        return $resp;
    }

    public function avatar   () { return 'dist/img/user2-160x160.jpg'; }

    public function user  () { return $this->request->user; }
    public function teams () { return []; }

    public function since () { return 'Nov. 2012'; }

    /***********************************************************************/

    public function subdomains () {
        #return array_keys($this->menus);

        return [
            'connect','hub',
            'dev','ops',#'cloud',
            'collab',
        ];
    }

    public function current () {
        return $this->menu(Application::curr()->context('silo'));
    }

    /***********************************************************************/

    public function menu ($sub) {
        if (!array_key_exists($sub, $this->menus)) {
            $pth = APP_PATH."/silos/{$sub}/menu.php";

            if (file_exists($pth)) {
                try {
                    $this->menus[$sub] = require_once($pth);
                } catch (Exception $ex) {
                    # Application::log()->error($ex);
                }
            }
        }

        if (array_key_exists($sub, $this->menus)) {
            return $this->menus[$sub];
        } else {
            return null;
        }
    }

    public function menus () {
        $curr = Application::curr()->context('silo');

        $resp = [];

        foreach ($this->subdomains() as $sub) {
            if ($sub!=$curr) {
                $mnu = $this->menu($sub);

                if ($mnu!=null) {
                    $resp[] = $mnu;
                }
            }
        }

        return $resp;
    }

    /***********************************************************************/

    public function messages () {
        return [[
            'when' => '5 mins',
            'sender' => [
                'pseudo' => "Support Team",
                'avatar' => 'dist/img/user2-160x160.jpg',
            ],
            'text' => "Why not buy a new awesome theme?",
        ],[
            'when' => '2 hours',
            'sender' => [
                'pseudo' => "AdminLTE Design Team",
                'avatar' => 'dist/img/user3-128x128.jpg',
            ],
            'text' => "Why not buy a new awesome theme?",
        ],[
            'when' => 'Today',
            'sender' => [
                'pseudo' => "Developers",
                'avatar' => 'dist/img/user4-128x128.jpg',
            ],
            'text' => "Why not buy a new awesome theme?",
        ],[
            'when' => 'Now',
            'sender' => [
                'pseudo' => "Sales",
                'avatar' => 'dist/img/user1-128x128.jpg',
            ],
            'text' => "Why not buy a new awesome theme?",
        ]];
    }

    /***********************************************************************/

    public function notifications () {
        return [[
            'icon' => 'users',
            'type' => 'aqua',
            'text' => "Langdon's Birthday",
            'link' => "#",
        ],[
            'icon' => 'warning',
            'type' => 'yellow',
            'text' => "Very long description here that may not fit into the page and may cause design problems",
            'link' => "#",
        ],[
            'icon' => 'users',
            'type' => 'red',
            'text' => "5 new members joined",
            'link' => "#",
        ],[
            'icon' => 'shopping-cart',
            'type' => 'green',
            'text' => "25 sales made",
            'link' => "#",
        ],[
            'icon' => 'user',
            'type' => 'red',
            'text' => "You changed your username",
            'link' => "#",
        ]];
    }

    /***********************************************************************/

    public function activity () {
        return [[
            'icon' => 'birthday-cake',
            'type' => 'red',
            'head' => "Langdon's Birthday",
            'body' => "Will be 23 on April 24th",
        ],[
            'icon' => 'user',
            'type' => 'yellow',
            'head' => "Frodo Updated His Profile",
            'body' => "New phone +1(800)555-1234",
        ],[
            'icon' => 'envelope-o',
            'type' => 'light-blue',
            'head' => "Nora Joined Mailing List",
            'body' => "nora@example.com",
        ],[
            'icon' => 'file-code-o',
            'type' => 'green',
            'head' => "Cron Job 254 Executed",
            'body' => "Execution time 5 seconds",
        ]];
    }

    /***********************************************************************/

    public function tasks () {
        return [[
            'icon' => 'globe',
            'type' => 'danger',
            'code' => 'red',
            'text' => "Custom Template Design",
            'curr' => 70, # %
        ],[
            'icon' => 'user',
            'type' => 'success',
            'code' => 'green',
            'text' => "Update Resume",
            'curr' => 95, # %
        ],[
            'icon' => 'code',
            'type' => 'warning',
            'code' => 'yellow',
            'text' => "Laravel Integration",
            'curr' => 50, # %
        ],[
            'icon' => 'database',
            'type' => 'primary',
            'code' => 'blue',
            'text' => "Back End Framework",
            'curr' => 68, # %
        ]];
    }
}

