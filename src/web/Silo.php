<?php

namespace PHP2USE\web;

/****************************************************************************/

class Silo extends Observable {
    private $cfg;

    public function __construct ($app, $cfg) {
        $this->cfg = $cfg;
    }

    /***********************************************************************/

    public static function ns      () { return $this->ns(); }

    public static function app     () { return $this->app(); }
    public static function cfg     () { return $this->cfg(); }
    public static function log     () { return $this->log(); }
    
    /***********************************************************************/

    public static function POST    () { return $this->trigger('app', 'post', func_get_args()); }
    public static function GET     () { return $this->trigger('app', 'get', func_get_args()); }
    public static function PUT     () { return $this->trigger('app', 'put', func_get_args()); }
    public static function DELETE  () { return $this->trigger('app', 'delete', func_get_args()); }
    public static function OPTIONS () { return $this->trigger('app', 'options', func_get_args()); }

    /***********************************************************************/

    protected function dynamic () {
        foreach ($this->$cfg['routing'] as $pth) {
            require_once($this->rpath('app', 'routes', "{$pth}.php"));
        }
    }
}

