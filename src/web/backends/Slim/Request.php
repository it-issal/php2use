<?php

namespace PHP2USE\web\backends\Slim;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

/****************************************************************************/

class Request extends \Slim\Http\Request {
    public $page;

    public function subdomain () { return APP_SUB; }
    public function domain    () { return APP_FQDN; }
}

