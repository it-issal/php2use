<?php

namespace PHP2USE\web\backends\Slim;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

/****************************************************************************/

class Middleware extends \Slim\Middleware {
    public function app () { return Application::curr(); }

    protected function trigger ($method, $args) {
        return call_user_func_array([$this, $method], $args);
    }

    public function call() {
        $this->trigger('before', [$this->app->request]);

        //Optionally call the next middleware
        $this->next->call();

        $this->trigger('after', [$this->app->request, $this->app->response]);
    }
}

