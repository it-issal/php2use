<?php

namespace PHP2USE\web\backends\Slim;

use PHP2USE\Reactor;

use PHP2USE\web\Application;
use PHP2USE\web\Page;
use PHP2USE\web\Identity;

use PHP2USE\web\helpers\Auth;
use PHP2USE\web\helpers\ACL;

use PHP2USE\ui\Dashboard;
use PHP2USE\ui\Block;
use PHP2USE\ui\Widget;

use PHP2USE\data\backends\graph\Manager as GraphDB;

/****************************************************************************/

class Gateway extends Application {
    public function php_ns ($name) { return "PHP2USE\\web\\backends\\Slim\\$name"; }
    
    /***********************************************************************/

    public function request  () { return $this->trigger('app', 'request',  func_get_args()); }
    public function response () { return $this->trigger('app', 'response', func_get_args()); }

    public function POST     () { return $this->trigger('app', 'post',     func_get_args()); }
    public function GET      () { return $this->trigger('app', 'get',      func_get_args()); }
    public function PUT      () { return $this->trigger('app', 'put',      func_get_args()); }
    public function DELETE   () { return $this->trigger('app', 'delete',   func_get_args()); }
    public function OPTIONS  () { return $this->trigger('app', 'options',  func_get_args()); }
    
    public function format   () { return $this->trigger('app', 'format',   func_get_args()); }
    public function flash    () { return $this->trigger('app', 'flash',    func_get_args()); }
    public function redirect () { return $this->trigger('app', 'redirect', func_get_args()); }

    /***********************************************************************/

    public function render ($view, $context=[]) {
        $context['request'] = $this->request();

        foreach (['page','identity'] as $field) {
            $context[$field] = $this->request()->$field;
        }

        return $this->app->render($view, $context);
    }

    public function respond ($payload) {
        $resp = new \Slim\Http\Response();

        $resp->setStatus(200);
        $resp->headers->set('Content-Type', 'application/json');

        $resp->write(json_encode($payload));

        $this->app->response = $resp;

        $this->app->response->finalize();
    }

    /***********************************************************************/

    protected function reg_ware ($mw) {
        $this->app()->add($mw);
    }

    /***********************************************************************/

    protected function reg_task ($task) {
        #$this->app()->add($mw);
    }

    /***********************************************************************/

    protected function setup () {
        $this->app = new \Slim\Slim(array(
            'log.writer' => new \Slim\Logger\DateTimeFileWriter(),
        ));

        $this->app->container->singleton('request', function ($c) {
            $req = new Request($c['environment']);

            $req->page     = new Page($req);
            $req->identity = new Identity($req);

            return $req;
        });

        $this->app->container->singleton('log', function () {
            return $this->log;
        });

        /*******************************************************************/

        $this->app->view(new View());
        $this->app->view->parserOptions = array(
            'charset' => 'utf-8',
            'cache' => Reactor::rpath('cache', 'templates'),
            'auto_reload' => true,
            'strict_variables' => false,
            'autoescape' => true,
        );

        $this->app->view->parserExtensions = [new \Slim\Views\TwigExtension()];

        foreach (['CDN'] as $key) {
            $this->app->view->parserExtensions[] = $this->php_ns("templates\\{$key}");
        }

        $this->app->view->twigTemplateDirs = array_merge(
            $this->inc['templates']['core'],
            $this->inc['templates']['app']
        );
    }

    /***********************************************************************/

    protected function lazy () {
        foreach ($this->inc['routing'] as $ns => $lst) {
            foreach ($lst as $nrw) {
                if (is_array($nrw)) {
                    $cls = "Silos\\{$this->context('silo')}\\views\\{$nrw['handler']}";

                    if (class_exists($cls)) {
                        $res = new $cls();

                        $mws = [];

                        foreach ($nrw['process'] as $mw) {
                            if (!array_key_exists('args', $mw)) {
                                $mw['args'] = [];
                            }

                            $obj = call_user_func_array([
                                "PHP2USE\\web\\helpers\\{$mw['type']}",$mw['name']
                            ], $mw['args']);

                            if ($obj!=null) {
                                $mws[] = $obj;
                            }
                        }

                        foreach ($cls::$VERBs as $verb) {
                            $target = null;

                            if (method_exists($res, $verb)) {
                                $target = [$res, $verb];
                            } else if (is_callable($res)) {
                                $target = $res;
                            }

                            if ($target!=null) {
                                call_user_func_array(['PHP2USE\\Reactor', $verb], array_merge([
                                    $nrw['pattern']
                                ],$mws,[
                                    $target
                                ]));
                            }
                        }
                    } else {
                        echo "Class '{$cls}' does'nt exist.";
                    }
                } else {
                    $pth = Reactor::rpath('app/silos', $this->context('silo'), 'views', "{$nrw}.php");

                    require($pth);
                }
            }
        }
    }

    /***********************************************************************/

    protected function process () {
        $this->app->run();
    }

    /***********************************************************************/

    protected function clean () {
    }
}

