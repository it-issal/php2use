<?php

namespace PHP2USE\web\backends\Slim;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

/****************************************************************************/

class Response extends \Slim\Http\Response {
    public function __construct($body = '', $status = 200, $headers = array()) {
        parent::__construct($body, $status, $headers);

        $this->cookies = new \PHP2USE\gateway\Slim\Cookies();
    }
}

