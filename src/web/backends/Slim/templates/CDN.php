<?php

namespace PHP2USE\web\backends\Slim\templates;

use Slim\Slim;

use PHP2USE\web\Application;

/****************************************************************************/

class CDN extends \Twig_Extension
{
    public function getName() { return 'cdn'; }

    public function getFunctions() {
        return array(
            new \Twig_SimpleFunction('static', array(Application::curr(), 'staticFor')),
            #new \Twig_SimpleFunction('urlFor', array($this, 'urlFor')),
        );
    }

    public function urlFor($name, $params = array(), $appName = 'default') {
        return Slim::getInstance($appName)->urlFor($name, $params);
    }
}

