<?php

namespace PHP2USE\web\backends\Gearman;

use PHP2USE\Reactor;
use PHP2USE\web\Application;
use PHP2USE\web\Page;
use PHP2USE\web\Identity;

/****************************************************************************/

class Gateway extends Application {
    public function php_ns ($name) { return "PHP2USE\\web\\backends\\Gearman\\$name"; }
    
    /***********************************************************************/

    public function request  () { return null; }
    public function response () { return null; }

    public function flash    () { ; }

    /***********************************************************************/

    public function render ($view, $context=[]) {
        $context['request'] = $this->request();

        foreach (['page','identity'] as $field) {
            $context[$field] = $this->request()->$field;
        }

        return $this->app->render($view, $context);
    }

    /****************************************************************/

    public function invoke ($alias, $args=[]) {
        if (array_key_exists($alias, $this->reg)) {
            return call_func_user_array([$this->reg[$alias],''], $args);
        } else {
            return null;
        }
    }

    /****************************************************************/

    protected function reg_ware ($mw) {
        #$this->app()->add($mw);
    }

    /***********************************************************************/

    public function reg_task ($task) {
        if (!array_key_exists($task->alias(), $this->reg)) {
            $this->reg[$task->alias()] = $task;
        }
    }

    /***********************************************************************/

    private $reg;
    private $host;
    private $worker;

    public function tasknames () { return array_keys($this->reg); }
    public function tasks     () { return array_values($this->reg); }
    public function host      () { return $this->host; }
    public function worker    () { return $this->worker; }

    protected function setup () {
        $this->host = '127.0.0.1';

        $this->reg = [];

        /********************************************************************/

        $this->worker = new \GearmanWorker();

        $this->worker->addServer($this->host);
    }

    /***********************************************************************/

    protected function lazy () {
        foreach ($this->inc['tasks'] as $ns => $lst) {
            foreach ($lst as $nrw) {
                if (is_array($nrw['path'])) {
                    $nrw['path'] = implode($nrw['path'], '\\');
                }

                $cls = null;

                switch ($nrw['type']) {
                    /*
                    case 'core':
                        $cls = $this->core_ns("middlewares\\{$nrw['path']}");
                        break;
                    //*/
                    case 'framework':
                        $cls = $this->php_ns("tasks\\{$nrw['path']}");
                        break;
                    case 'application':
                        $cls = $this->app_ns("tasks\\{$nrw['path']}");
                        break;
                    case 'custom':
                    #default:
                        $cls = $nrw['path'];
                        break;
                }

                if (class_exists($cls)) {
                    $this->reg_task(new $cls());
                }
            }
        }
    }

    /***********************************************************************/

    protected function process () {
        foreach ($this->reg as $alias => $task) {
            $this->worker->addFunction($alias, $task);
        }

        while (1) {
            print "Waiting for job...\n";

            $ret= $this->worker->work();

            if ($this->worker->returnCode() != GEARMAN_SUCCESS) break;
        }
    }

    /***********************************************************************/

    protected function clean () {
    }
}

