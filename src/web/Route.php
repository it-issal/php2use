<?php

namespace PHP2USE\web;

class Route {
    private $fqdn;
    public  $target;
    public  $callback;
    private $theme;
    private $scopes;

    public function __construct ($target, $callback, $theme, $fqdn, $scopes, $any) {
        $this->fqdn     = $fqdn;
        $this->target   = $target;
        $this->callback = $callback;
        $this->theme    = $theme;
        $this->scopes   = $scopes;
        $this->any      = $any;
    }

    public function is_active () {
        $req = Site::platform()->request();

        if ($this->fqdn!=null and $this->fqdn!=$req->base) {
            return false;
        }

        if (sizeof($this->scopes) and !(SSO\Manager::has_scopes($this->scopes, $this->any))) {
            return false;
        }

        return true;
    }

    public function __invoke() {
        $params = func_get_args();

        Site::platform()->set_theme($this->theme);

        return call_user_func_array($this->callback, $params);
    }
}

