<?php

namespace PHP2USE\web;

use PHP2USE\core\Observable;

use PHP2USE\web\Application;

/****************************************************************************/

class Page extends Observable {
    public static function curr () { return Application::curr()->request()->page; }

    /***********************************************************************/

    private $request;

    public function __construct ($req) {
        $this->request    = $req;

        $this->branding   = 'IT issal';
        $this->title      = 'Some web page';
        $this->subtitle   = '';

        $this->heading    = '<b>IT</b> issal';

        $this->skin       = 'blue';
        $this->breadcrumb = [];
    }

    /***********************************************************************/

    public $branding;
    public $title;
    public $subtitle;

    public $headline;
    public $heading;

    public $skin;
    public $breadcrumb;

    /***********************************************************************/

    #public function ns      () { return $this->ns(); }

    public function app  () { return Application::curr(); }
    public function silo () { return ucfirst($this->app()->context('silo')); }

    public function headline () {
        return "[{$this->silo()}] {$this->title} - {$this->branding}";
    }
}

