<?php

namespace PHP2USE\web\helpers;

class HTML5 extends Common\Module {
    public static function render ($path, $params=array()) {
        echo Site::render_tpl($path, $params, false);
    }
    
    /**************************************************************************/
    
    public static function bootstrap () {
        
    }
    
    /**************************************************************************/
    
    private static function call_targets () {
        $stk = array('PHP2USE\\Site');
        
		return $stk;
	}
    
    /**************************************************************************/
    
    public static function xml_ns () {
        return 'lang="en-US" xmlns:og="http://ogp.me/ns#"';
    }
    
    public static function header () {
        foreach (HTML5::call_targets() as $cls) {
            forward_static_call(array($cls, 'html5_header'));
        }
		
		foreach (Site::apis() as $api) {
            forward_static_call(array($api, 'html5_header'));
        }
    }
    public static function footer () {
        foreach (HTML5::call_targets() as $cls) {
            forward_static_call(array($cls, 'html5_footer'));
        }
		
		foreach (Site::apis() as $api) {
            forward_static_call(array($api, 'html5_header'));
        }
    }
    
    /**************************************************************************/
    
    private static function dyntag ($tag, $params) {
        $val  = $params[0];
        $args = $params[1];
        
        return "<{$tag} {$args}>{$val}</{$tag}>";
    }
    
    /**************************************************************************/
    
    public static function span   () { return HTML5::dyntag('span', func_get_args()); }
    public static function h1     () { return HTML5::dyntag('h1',   func_get_args()); }
    public static function h2     () { return HTML5::dyntag('h2',   func_get_args()); }
    public static function h3     () { return HTML5::dyntag('h3',   func_get_args()); }
    public static function h4     () { return HTML5::dyntag('h4',   func_get_args()); }
    public static function h5     () { return HTML5::dyntag('h5',   func_get_args()); }
    public static function h6     () { return HTML5::dyntag('h6',   func_get_args()); }
    
    public static function ulist () {
        
    }
    public static function olist () {
        
    }
    
    public static function img () {
        
    }
}

class H5 extends HTML5 {
    public static function link ($target, $label) {
        echo "<a target='blank' href='{$target}'>{$label}</a>";
    }
}

