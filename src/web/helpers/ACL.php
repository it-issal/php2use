<?php

namespace PHP2USE\web\helpers;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

/****************************************************************************/

Auth::tenant('account', function ($value) {
    if ($value!=null) {
        return Auth::profile($value);
    } else {
        return null;
    }
});

Auth::tenant('organization', function ($value) {
    if ($value!=null) {
        return $value;
    } else {
        return null;
    }
});

/****************************************************************************/

class ACL {
    public static function must_login () {
        $app = Application::curr();

        // $app->flash('error', 'Login required');

        //print_r($_SERVER);die(1);

        $app->redirect("{$app->settings('login_url')}?next=http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
    }

    /************************************************************************/

    public static function anon () {
        return function () {
            if (Auth::is_anon()) {
                #ACL::must_login();
            }
        };
    }

    public static function logged () {
        return function () {
            Reactor::respond([
                'error' => 'auth.login',
                'session' => $_SESSION,
                'args' => $_POST,
            ]);

            if (Auth::is_anon()) {
                //ACL::must_login();
            }
        };
    }

    public static function role ($group) {
        return function () use ( $group ) {
            if (!Auth::has_role($group)) {
                ACL::must_login();
            }
        };
    }

    public static function perm ($type, $action) {
        return function () use ( $type, $action ) {
            if (!Auth::has_perm($type, $action)) {
                ACL::must_login();
            }
        };
    }
}

/*
Reactor::GET('/foo', Auth::has_role('admin'), function () {
    //Display admin control panel
});
//*/

