<?php

namespace PHP2USE\web\helpers;

class WebTools extends Common\Component {
    private static $domain_tools = array(
        array(
            'icon' => '/assets/badges/ipv6.png', 'title' => "Google PageSpeed Insights [desktop]",
            'link' => "http://ip6.nl/#!{narrow}",
        ),
    );
    
    private static $url_tools = array(
        array(
            'icon' => '/assets/badges/w3c.png', 'title' => "W3C Validator",
            'link' => "http://validator.w3.org/check?uri=http%3A%2F%2F{narrow}&charset=%28detect+automatically%29&doctype=Inline&group=0",
        ),
        array(
            'icon' => '/assets/badges/html5.png', 'title' => "HTML 5 Validator",
            'link' => "http://html5.validator.nu/?doc=http%3A%2F%2F{narrow}%2F",
        ),
        array(
            'icon' => 'chart', 'title' => "Google PageSpeed Insights [desktop]",
            'link' => "http://developers.google.com/speed/pagespeed/insights/?url={narrow}&tab=desktop",
        ),
        array(
            'icon' => 'mobile', 'title' => "Google PageSpeed Insights [mobile]",
            'link' => "http://developers.google.com/speed/pagespeed/insights/?url={narrow}&tab=mobile",
        ),
        array(
            'icon' => 'google', 'title' => "Google RichSnippets",
            'link' => "http://www.google.com/webmasters/tools/richsnippets?q={narrow}",
        ),
    );
    
    public static function domain ($target) {
        $resp = array();
        
        foreach (Manager::$domain_tools as $entry) {
            $resp[] = array(
                'title' => $entry['title'],
                'icon'  => $entry['icon'],
                'link'  => str_replace('{narrow}', $target, $entry['link']),
            );
        }
        
        return $resp;
    }
    public static function url ($target) {
        $resp = array();
        
        foreach (Manager::$url_tools as $entry) {
            $resp[] = array(
                'title' => $entry['title'],
                'icon'  => $entry['icon'],
                'link'  => str_replace('{narrow}', $target, $entry['link']),
            );
        }
        
        return $resp;
    }
}

