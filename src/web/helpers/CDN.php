<?php

namespace PHP2USE\web\helpers;

use PHP2USE\Common as Common;

class Manager extends Common\Module {
    private static $config = null;
    
    /***************************************************************************************************/
    
    public static function bootstrap () {
        $path = 'app/specs/cdn.yml';
        
        if (file_exists($path)) {
            Manager::$config = yaml_loads($path);
        } else {
            Manager::$config = array(
                'assemblies' => array(
                    'fonts' => array(
                        
                    ),
                    'styles' => array(
                        
                    ),
                    'scripts' => array(
                        
                    ),
                ),
            );
        }
        
        foreach (array(
            'styles'  => array('slot' => 'head'),
            'scripts' => array('slot' => 'foot'),
        ) as $nrw => $defaults) {
            for ($i=0 ; $i<sizeof(Manager::$config[$nrw]) ; $i++) {
                foreach ($defaults as $key => $value) {
                    if (!array_key_exists($key, Manager::$config[$nrw][$i])) {
                        Manager::$config[$nrw][$i][$key] = $value;
                    }
                    
                    if (!in_array(Manager::$config[$nrw][$i][$key], array('head','foot','alpha','none'))) {
                        Manager::$config[$nrw][$i][$key] = $value;
                    }
                }
                
                if (array_key_exists('url', Manager::$config[$nrw])) {
                    Manager::$config[$nrw]['link'] = Manager::$config[$nrw]['url'];
                } else if (array_key_exists('path', Manager::$config[$nrw])) {
                    $prefix = '/assets/';
                    
                    switch ($nrw) {
                        case 'styles':
                            $prefix .= 'css';
                            break;
                        case 'scripts':
                            $prefix .= 'js';
                            break;
                    }
                    
                    Manager::$config[$nrw]['link'] = $prefix.'/'.Manager::$config[$nrw]['path'];
                }
            }
        }
        
        if (!file_exists($path)) {
            $f = open($path, 'w+');
            
            $f->write(yaml_dumps(Manager::$config));
            
            $f->close();
        }
    }
    
    /***************************************************************************************************/
    
    private static function html5_assemblies ($slot) {
        foreach (Manager::$config['styles'] as $entry) {
            if ($entry['slot']==$slot) {
                echo "<link rel='stylesheet' href='{$entry['link']}' />";
            }
        }
        
        foreach (Manager::$config['scripts'] as $entry) {
            if ($entry['slot']==$slot) {
                echo "<script src='{$entry['link']}'></script>";
            }
        }
    }
    
    public static function html5_header () {
        Manager::html5_assemblies('head');
    }
    
    public static function html5_footer () {
        Manager::html5_assemblies('foot');
    }
    
    /***************************************************************************************************/
    
    public static function connect () {
        
    }
}

