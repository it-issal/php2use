<?php

namespace PHP2USE\web\helpers;

class i18n {
    private static $cache = null;
    
    public static function bootstrap () {
        i18n::$cache = null;
    }
    
    public static function reflect ($key) {
        if (Site::has($key)) {
            return i18n::trans(Site::get($key));
        } else {
            return "";
        }
    }
    
    public static function trans ($verb, $lang=null, $text=null) {
        if ($lang==null) {
            $lang = Site::get('site.lang');
        }
        
        $resp = R::find('i18n_trans', 'verb=? and lang=?', array($verb, $lang));
        
        if (sizeof($resp)) {
            $resp = $resp[0];
        } else {
            if ($text!=null) {
                $resp = R::dispense('i18n_trans');
                
                $resp->verb = $verb;
                $resp->lang = $lang;
                $resp->text = $text;
                
                R::store($resp);
            } else {
                $resp = null;
            }
        }
        
        if ($text!=null) {
            return $resp->text;
        } else {
            return $verb;
        }
    }
    
    public static function query ($verb=null, $lang=null) {
        $resp = array();
        
        foreach (R::findAll('i18n_trans') as $entry) {
            $resp[] = $entry;
        }
        
        return $resp;
    }
}

