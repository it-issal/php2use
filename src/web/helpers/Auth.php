<?php

namespace PHP2USE\web\helpers;

use PHP2USE\Reactor;

/****************************************************************************/

class Auth {
    private static $tenancy = [];

    public static function tenant ($pivot, $callback) {
        if (!array_key_exists($pivot, Auth::$tenancy)) {
            Auth::$tenancy[$pivot] = $callback;
        }
    }

    /************************************************************************/

    public static function bootstrap () {
        $cnx = parse_url(APP_MONGO);

        /*******************************************************************/

        /*
        MongoSession::config(array(
            'connection'    => APP_MONGO,
            //'connection'    => "mongodb://{$cnx['host']}:{$cnx['port']}",
            //'username'      => $cnx['user'],
            //'password'      => $cnx['pass'],
            'db'            => substr($cnx['path'], 1),
            'cookie_domain' => '.'+APP_FQDN,
        ));
        //*/

        //MongoSession::init();
    }

    /************************************************************************/

    private static $registry = [
        [
            'user'    => 'shivha',
            'passwd'  => 'test',
            'email'   => 'tayamino@gmail.com',
            'profile' => [
                'full'   => 'TAYAA Med Amine',
                'role'   => ['super','dev','ops'],
            ],
        ],
    ];

    /************************************************************************/

    protected static function credentials ($login, $passwd) {
        foreach (Auth::$registry as $identity) {
            if ($identity['user']==$login or $identity['email']==$login) {
                if ($passwd==null or $identity['passwd']==$passwd) {
                    return $identity;
                }
            }
        }

        if ($passwd!=null) {
            Reactor::flashNow('auth', "Username of password wrong !");
        }

        return null;
    }

    /************************************************************************/

    protected static function profile ($narrow) {
        $identity = Auth::credentials($narrow, null);

        if ($identity!=null) {
            if ($identity['user']==$narrow or $identity['email']==$narrow) {
                $resp = [];

                foreach (['user', 'email'] as $k) {
                    $resp[$k] = $identity[$k];
                }

                foreach ($identity['profile'] as $k => $v) {
                    $resp[$k] = $v;
                }

                return $resp;
            }
        }

        return null;
    }

    /************************************************************************/

    public static function login ($user, $passwd) {
        $data = Auth::credentials($user, $passwd);

        if ($data!=null) {
            $_SESSION['account'] = $user;
            $_SESSION['profile'] = $data;

            return true;
        } else {
            return false;
        }
    }

    public static function logout () {
        foreach (['account','profile'] as $key) {
            $_SESSION[$key] = null;

            unset($_SESSION[$key]);
        }

        return Auth::is_anon();
    }

    /************************************************************************/

    public static function is_anon () {
        if (isset($_SESSION['account'])) {
            if ($_SESSION['account']!=null) {
                return false;
            }
        }

        return true;
    }

    public static function user () {
        if (isset($_SESSION['account'])) {
            if ($_SESSION['account']!=null) {
                return Auth::profile($_SESSION['account']);
            }
        }

        return null;
    }

    /************************************************************************/

    public static function has_role ($role) {
        if (!Auth::is_anon()) {
            $user = Auth::user();

            if (array_key_exists('groups', $user)) {
                if ( in_array($role, $user['groups']) ) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function has_perm ($type, $action) {
        if (!Auth::is_anon()) {
            $user = Auth::profile();

            if (array_key_exists('perms', $user)) {
                if (array_key_exists($type, $user['perms'])) {
                    if ( in_array($role, $user['perms'][$type]) ) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}

