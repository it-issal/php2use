<?php

namespace PHP2USE\web;

use PHP2USE\Reactor;
use PHP2USE\core\Observable;

use PHP2USE\api\Cloud;
use PHP2USE\api\Endpoint;

/****************************************************************************/

class Application extends Observable {
    protected static $curr = null;

    public static function curr () { return Application::$curr; }

    public static function bootstrap ($domain=null, $framework='slim') {
        $app = Application::check($domain, $framework);
        
        if ($app!=null) {
            Application::$curr = $app;

            return $app;
        } else {
            # Error
        }
    }

    /***********************************************************************/

    protected static function check ($domain, $framework) {
        $env = Reactor::load_yaml('app','config.yml');

        if ($domain==null) {
            $env['context'] = [
                'host'  => null,
                'fqdn'  => null,
                'silo'  => null,
            ];

            $prvd = ucfirst($framework);

            $cls = "\\PHP2USE\\web\\backends\\{$prvd}\\Gateway";

            return new $cls($env);
        } else {
            $temp = explode('.', $domain, 2);

            $default = $env['settings']['default_site'];
            $cookie = $env['settings']['cookies.domain'];

            $silo = $temp[0];
            $fqdn = $temp[1];

            $env['context'] = [
                'host'  => $domain,
                'fqdn'  => $fqdn,
                'silo'  => $silo,
            ];

            if (($cookie==".{$fqdn}") or ($domain==$default)) {
                if (Reactor::is_dir('app/silos', $silo, 'views')) {
                    $prvd = ucfirst($framework);

                    $cls = "\\PHP2USE\\web\\backends\\{$prvd}\\Gateway";

                    return new $cls($env);
                }
            } else {
                print_r([
                    'dns' => $temp,
                    'env' => $env,
                ]);die();
            }
	    }

	    return null;
    }

    private static function match ($domain, $pattern) {
        if (is_array($pattern)) {
            foreach ($pattern as $target) {
                return Application::match($domain, $target);
            }

            return false;
        } else {
            return $domain==$pattern;
        }
    }

    /***********************************************************************/

    public function ns       () { return $this->ns; }

    public function app      () { return $this->app; }
    public function cfg      () { return $this->cfg; }
    public function log      () { return $this->log; }

    public function queue    () { return $this->queue; }

    /***********************************************************************/

    protected $theme;

    public function staticFor($path) {
        return "/themes/{$this->theme}/static/{$path}";
    }

    public function gohome() {
        $link = '/';

        if (isset($_GET['next'])) {
            $link = $_GET['next'];
        }

        //Reactor::respond(['ok' => print_r($_SESSION,true)]);

        Reactor::redirect($link);
    }

    /***********************************************************************/

    protected $app;
    protected $log;
    protected $queue;

    protected $ns;
    protected $inc;

    protected $cfg;
    protected $stt;
    protected $cnt;

    private function __construct ($env) {
        $this->cfg = [];
        $this->stt = [];

        $this->cloud     = [];
        $this->endpoints = [];

        $this->inc = [
            'helpers'     => ['core' => []],
            'middlewares' => ['core' => []],
            'routing'     => ['core' => [], 'app' => []],
            'tasks'       => ['core' => [], 'app' => []],
            'templates'   => ['core' => [], 'app' => []],
        ];

        $alias = [
            'namespace' => 'ns',
            'context'   => 'cnt',
            'settings'  => 'stt',
        ];

        foreach ($env as $k => $v) {
            switch ($k) {
                case 'namespace':
                case 'theme':
                case 'context':
                case 'settings':
                    $prop = $alias[$k] or $k; $this->$prop = $v;
                    break;
                case 'helpers':
                case 'middlewares':
                case 'tasks':
                case 'routing':
                    $this->inc[$k]['app'] = $v;
                    break;
                default:
                    $this->cfg[$k] = $v;
                    break;
            }
        }

        /*******************************************************************/

        $routing = Reactor::load_yaml('app','routing.yml');

        foreach ($routing as $ind => $cfg) {
            if (Application::match($this->cnt['host'], $cfg['regex'])) {
                $this->theme = $cfg['theme'];

                foreach ($cfg['views'] as $view) {
                    $this->inc['routing']['app'][] = $view;
                }
            }
        }

        /*******************************************************************/

        $this->inc['templates']['core'][] = Reactor::rpath('themes', $this->theme, 'views');

        $this->inc['templates']['app'] = [];

        foreach (['templates'] as $key) { # ['models', 'views', 'templates']
            if (Reactor::is_dir('app/silos', $this->cnt['silo'], $key)) {
                $this->inc['templates']['app'][] = Reactor::rpath('app/silos', $this->cnt['silo'], $key);
            }
        }

        $this->inc['templates']['app'][] = Reactor::rpath('app','templates');

        /*******************************************************************/

        $this->log = new \Monolog\Logger($this->ns());
        $this->log->pushHandler(new \Monolog\Handler\StreamHandler(Reactor::rpath('cache', 'app.log'), \Monolog\Logger::DEBUG));

        /*******************************************************************/

        $this->queue = null;

        /*******************************************************************/

        $this->trigger(null, 'setup');

        $this->trigger(null, 'dynamic');

        /*
        $target = null;

        foreach ($routing as $route) {
            $cnt = Reactor::check_silo($_SERVER["HTTP_HOST"]);

            if ($cnt!=null) {
                $target = $cnt;
            }
        }

        if ($target==null) {
            $target = Reactor::check_silo($this->stt['default_site']);
        }

        if ($target!=null) {
            foreach ($target as $k => $v) {
                if (in_array($k, ['name', 'host', 'fqdn', 'silo', 'skin'])) {
                    define('APP_'.strtoupper($k), $v);
                }
            }

            foreach ($this->inc['routes'] as $md) {
                require_once(Reactor::rpath('app/silos', $this->cnt['silo'], "{$md}.php"));
            }
        } else {
            
        }
        //*/
    }

    /***********************************************************************/

    public function run () {
        $this->trigger(null, 'lazy');

        $this->trigger('silo', 'before', [$this->request()]);

        $this->process($this->request());

        $this->trigger('silo', 'after', [$this->request(), $this->response()]);

        $this->trigger(null, 'clean');
    }

    /***********************************************************************/

    public function context ($key) {
        if (array_key_exists($key, $this->cnt)) {
            return $this->cnt[$key];
        } else {
            return null;
        }
    }

    public function config ($key) {
        if (array_key_exists($key, $this->cfg)) {
            return $this->cfg[$key];
        } else {
            return null;
        }
    }

    public function settings ($key) {
        if (array_key_exists($key, $this->stt)) {
            return $this->stt[$key];
        } else {
            return null;
        }
    }

    /***********************************************************************/

    #public function core_ns ($name) { return "PHP2USE\\web\\backends\\Slim\\$name"; }
    public function app_ns ($name)  {
        require_once(APP_PATH."/middlewares/{$name}.php");

        return "{$name}Stack";
    }
    
    protected function dynamic () {
        foreach ($this->inc['helpers'] as $ns => $lst) {
            foreach ($lst as $key) {
                require_once(Reactor::rpath($ns, 'helpers', "{$key}.php"));
            }
        }

        /*******************************************************************/

        foreach ($this->inc['middlewares'] as $ns => $lst) {
            foreach ($lst as $nrw) {
                if (is_array($nrw['path'])) {
                    $nrw['path'] = implode($nrw['path'], '\\');
                }

                $cls = null;

                switch ($nrw['type']) {
                    /*
                    case 'core':
                        $cls = $this->core_ns("middlewares\\{$nrw['path']}");
                        break;
                    //*/
                    case 'framework':
                        $cls = $this->php_ns("middlewares\\{$nrw['path']}");
                        break;
                    case 'application':
                        $cls = $this->app_ns($nrw['path']);
                        break;
                }

                if (class_exists($cls)) {
                    $this->reg_ware(new $cls());
                }
            }
        }

        /*******************************************************************/

        /*
        foreach ($this->inc['tasks'] as $ns => $lst) {
            foreach ($lst as $nrw) {
                if (is_array($nrw['path'])) {
                    $nrw['path'] = implode($nrw['path'], '\\');
                }

                $cls = null;

                switch ($nrw['type']) {
                    case 'core':
                        $cls = $this->core_ns("tasks\\{$nrw['path']}");
                        break;
                    case 'framework':
                        $cls = $this->php_ns("tasks\\{$nrw['path']}");
                        break;
                    case 'application':
                        $cls = $this->app_ns("tasks\\{$nrw['path']}");
                        break;
                    case 'custom':
                    #default:
                        $cls = $nrw['path'];
                        break;
                }

                if (class_exists($cls)) {
                    $this->reg_task(new $cls());
                }
            }
        }
        //*/
    }

    /***********************************************************************/

    public function apis () {
        return array_merge($this->clouds(), $this->endpoints());
    }

    /***********************************************************************/

    protected $cloud;

    public function clouds () {
        return array_values($this->cloud);
    }

    public function cloud ($provider) {
        if (array_key_exists($provider, $this->cloud)) {
            return $this->cloud[$provider];
        }

        return null;
    }

    public function connect ($provider, $config=[], $creds=[], $vault=[]) {
        if (!array_key_exists($provider, $this->cloud)) {
            $resp = Cloud::instance([$provider], $config, $creds, $vault);

            $this->cloud[$provider] = $resp;
        }
        
        return $this->cloud[$provider];
    }

    /***********************************************************************/

    protected $endpoints;

    public function endpoints () {
        $resp = [];

        foreach ($this->endpoints as $prvd => $lst) {
            $resp = array_merge($resp, array_values($lst));
        }

        return $resp;
    }

    public function endpoint ($provider, $alias='default') {
        if (array_key_exists($provider, $this->endpoints)) {
            if (array_key_exists($alias, $this->endpoints[$provider])) {
                return $this->endpoints[$provider][$alias];
            }
        }

        return null;
    }

    public function plug ($provider, $alias='default', $config=[], $creds=[], $vault=[]) {
        if (!array_key_exists($provider, $this->endpoints)) {
            $this->endpoints[$provider] = [];
        }

        if (!array_key_exists($alias, $this->endpoints[$provider])) {
            $resp = Endpoint::instance([$provider, $alias], $config, $creds, $vault);

            $this->endpoints[$provider][$alias] = $resp;
        }
        
        return $this->endpoints[$provider][$alias];
    }
}

