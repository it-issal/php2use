<?php

namespace PHP2USE\ui;

use PHP2USE\web\Application;

/****************************************************************************/

class Control {
    protected static $bs_cols = ['md', 'lg', 'sm', 'xs'];

    protected $kind;
    protected $name;
    protected $args;

    public $title;

    protected function __construct ($kind, $name, $args=[]) {
        $this->kind = $kind;
        $this->name = $name;

        $this->args = [];

        $fields = array_merge(['title','style','data'], $this->required, array_keys($this->defaults));

        foreach ($fields as $k) {
            if (array_key_exists($k, $args)) {
                $v = $args[$k];

                if (in_array($k, $fields)) {
                    $this->$k = $v;
                } else {
                    $this->args[$k] = $v;
                }
            }
        }

        $this->setup();
    }

    public function name () { return $this->name; }

    /************************************************************************/

    public function render () {
        $resp = Application::curr()->render($this->template(), $this->context());

        return $resp;
    }
}

