<?php

namespace PHP2USE\ui;

use PHP2USE\Reactor;

use PHP2USE\web\Application;
use PHP2USE\web\Page;

use PHP2USE\web\helpers\Auth;
use PHP2USE\web\helpers\ACL;

use PHP2USE\ui\Dashboard;
use PHP2USE\ui\Block;
use PHP2USE\ui\Widget;

use PHP2USE\data\backends\graph\Manager as GraphDB;

/****************************************************************************/

class Dashboard implements \ArrayAccess {
    public static $VERBs = ['GET','POST'];
    public static $layouts = [
        'layout-3' => ['begin','main','finish'],
        'layout-4' => ['north','east','west','south'],
        'side-r'   => ['north','east','west','south'],
        'side-l'   => ['north','east','west','south'],
    ];

    protected $template;
    protected $page;
    protected $zones;

    public $context;

    public function __construct ($parent=null, $layout='layout-4') {
        if ($parent==null) {
            $parent = Page::curr();
        }

        $this->page     = $parent;
        $this->context  = [];

        $this->layout   = null;
        $this->template = null;

        $this->reset($layout);
    }

    public function page () { return $this->page; }

    public function app  () { return $this->page()->app(); }
    public function log  () { return $this->app()->log(); }

    /********************************************************************/

    protected function reset ($key=null, $tpl=null) {
        if ($key!=null) {
            if ($this->layout!=$key) {
                $this->zones  = [];

                if (array_key_exists($key, Dashboard::$layouts)) {
                    foreach (Dashboard::$layouts[$key] as $nrw) {
                        $this->zones[$nrw] = [];
                    }
                }
            }

            $this->layout = $key;
        }

        if ($tpl!=null) {
            $this->template = $tpl;
        }
    }

    /********************************************************************/

    protected function setup () {
    }
    
    /********************************************************************/
    
    public function template () { return "dashboard/{$this->layout}.html"; }
    
    /********************************************************************/
    
    protected function get_template ($curr=null) {
        if ($curr!=null) {
            return $curr;
        }

        if ($this->template!=null) {
            return $this->template;
        }

        return "dashboard/{$this->layout}.html";
    }

    /********************************************************************/
    
    public function zone ($key) {
        if (array_key_exists($key, $this->zones)) {
            return array_values($this->zones[$key]);
        } else {
            return null;
        }
    }

    /********************************************************************/
    
    public function block () {
        $args = func_get_args();

        $zone = $args[0]; $args = array_slice($args, 1);

        $obj = call_user_func_array(['\\PHP2USE\\ui\\Block', 'instance'], $args);

        if ($obj!=null) {
            if (!in_array($obj, $this->zones[$zone])) {
                $this->zones[$zone][] = $obj;
            }
        }

        return $obj;
    }

    public function widget () {
        $args = func_get_args();

        $zone = $args[0]; $args = array_slice($args, 1);

        $obj = call_user_func_array(['\\PHP2USE\\ui\\Widget', 'instance'], $args);

        if ($obj!=null) {
            if (!in_array($obj, $this->zones[$zone])) {
                $this->zones[$zone][] = $obj;
            }
        }

        return $obj;
    }

    /********************************************************************/

    protected function populate () {
        return [];
    }

    /********************************************************************/
    
    public function render ($template=null) {
        $resp = array_merge([
            'dashboard' => $this,
            'controls'  => $this->zones,
        ], $this->context, $this->populate());

        /*
        Reactor::format('application/json', function () use ($resp) {
            Reactor::render_json($resp);
        });

        Reactor::format('text/xml', function () use ($resp) {
            Reactor::render_xml($resp);
        });

        Reactor::format('text/html', function () use ($resp) {
            //*/
            Reactor::render($this->get_template($template), $resp);
            /*
        });
        //*/
    }

    /********************************************************************/
    
    protected function configure () {

    }

    /********************************************************************/
    
    public function __invoke () {
        $this->configure();

        $this->render(null, [
            'params' => func_get_args(),
        ]);
    }

    /********************************************************************/

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->context[] = $value;
        } else {
            $this->context[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        $resp = array_merge($this->context, $this->populate());

        if (array_key_exists($offset, $resp)) {
            return true;
        } else {
            return false;
        }
    }

    public function offsetUnset($offset) {
        unset($this->context[$offset]);
    }

    public function offsetGet($offset) {
        $resp = array_merge($this->context, $this->populate());

        if (array_key_exists($offset, $resp)) {
            return $resp[$offset];
        } else {
            return null;
        }
    }
}

