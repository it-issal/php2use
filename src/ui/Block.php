<?php

namespace PHP2USE\ui;

use PHP2USE\web\Application;

/****************************************************************************/

class Block extends Control {
    public static function instance ($kind, $name, $args=[]) {
        $cls = "\\PHP2USE\\ui\\blocks\\{$kind}";

        return new $cls($kind, $name, $args);
    }

    /************************************************************************/

    public $style = [
        
    ];

    protected function template () { return "blocks/{$this->kind}.html"; }

    protected function context () {
        $resp = [
            'widget' => $this,
            'style'  => $this->style,
        ];

        return $resp;
    }

    /************************************************************************/

    protected function before () {
        
    }

    /************************************************************************/

    protected function after () {
        
    }
}

