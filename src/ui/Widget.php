<?php

namespace PHP2USE\ui;

use PHP2USE\core\Application;

/****************************************************************************/

class Widget extends Block {
    public static function instance ($kind, $name, $args=[]) {
        $cls = "\\PHP2USE\\ui\\widgets\\{$kind}";

        return new $cls($kind, $name, $args);
    }

    /************************************************************************/

    public $cube;
    public $icon;

    public $style = [
        
    ];

    /************************************************************************/

    protected function template () { return "widgets/{$this->kind}.html"; }

    protected function context () {
        $resp = [
            'widget' => $this,
            'style'  => $this->style,
            'data'   => $this->populate(),
        ];

        return $resp;
    }

    /************************************************************************/

    protected function setup () {

    }
}

