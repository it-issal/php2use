<?php

namespace PHP2USE\ui\widgets;

use PHP2USE\ui\Widget;

/****************************************************************************/

class Graph extends Widget {
    public $data;

    /************************************************************************/

    protected $required = ['data'];
    protected $defaults = [
        'icon'  => 'th',
        'style' => [
            'main' => 'box-info',
            'body' => 'border-radius-none',
        ],
    ];

    /************************************************************************/

    protected function populate () {
        return [
            'nodes' => $this->data->nodes(),
            'edges' => $this->data->edges(),
        ];
    }
}

