<?php

namespace PHP2USE\ui\widgets;

use PHP2USE\ui\Widget;

/****************************************************************************/

class Charts extends Widget {
    #public $value;

    /************************************************************************/

    protected $required = [];
    protected $defaults = [
        'icon'  => 'th',
        'style' => [
            'main' => 'box-info',
            'body' => 'border-radius-none',
        ],
    ];

    /************************************************************************/

    protected function populate () {
        return [
            
        ];
    }
}

