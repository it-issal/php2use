<?php

namespace PHP2USE\ui\widgets;

use PHP2USE\ui\Widget;

/****************************************************************************/

class Map extends Widget {
    #public $value;

    /************************************************************************/

    protected $required = [];
    protected $defaults = [
        'icon'  => 'map-marker',
    ];

    public $style = [
        'main' => 'box-solid bg-light-blue-gradient',
        'body' => '',
    ];

    /************************************************************************/

    protected function populate () {
        return [
            
        ];
    }
}

