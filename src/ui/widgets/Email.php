<?php

namespace PHP2USE\ui\widgets;

use PHP2USE\ui\Widget;

/****************************************************************************/

class Email extends Widget {
    #public $value;

    /************************************************************************/

    protected $required = [];
    protected $defaults = [
        'icon'  => 'envelope',
    ];

    public $style = [
        'main' => 'box-info',
        'body' => '',
    ];

    /************************************************************************/

    protected function populate () {
        return [
            
        ];
    }
}

