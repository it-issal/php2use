<?php

namespace PHP2USE\ui\widgets;

use PHP2USE\ui\Widget;

/****************************************************************************/

class Chat extends Widget {
    #public $value;

    /************************************************************************/

    protected $required = [];
    protected $defaults = [
        'icon'  => 'comments-o',
    ];

    public $style = [
        'main' => 'box-success',
        'body' => 'chat',
    ];

    /************************************************************************/

    protected function populate () {
        return [
            'feed' => [[
                'when' => '2:15',
                'text' => "I would like to meet you to discuss the latest news about the arrival of the new theme. They say it is going to be one the best themes on the market",
                'user' => [
                    'pseudo' => "Mike Doe",
                    'avatar' => 'dist/img/user4-128x128.jpg',
                ],
                'file' => [
                    ['path' => 'Theme-thumbnail-image.jpg'],
                ],
            ],[
                'when' => '5:15',
                'text' => "I would like to meet you to discuss the latest news about the arrival of the new theme. They say it is going to be one the best themes on the market",
                'user' => [
                    'pseudo' => "Alexander Pierce",
                    'avatar' => 'dist/img/user3-128x128.jpg',
                ],
            ],[
                'when' => '5:30',
                'text' => "I would like to meet you to discuss the latest news about the arrival of the new theme. They say it is going to be one the best themes on the market",
                'user' => [
                    'pseudo' => "Susan Doe",
                    'avatar' => 'dist/img/user2-160x160.jpg',
                ],
            ]],
        ];
    }
}

