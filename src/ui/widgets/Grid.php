<?php

namespace PHP2USE\ui\widgets;

use PHP2USE\ui\Widget;

/****************************************************************************/

class Grid extends Widget {
    public $data;

    /************************************************************************/

    protected $required = ['data'];
    protected $defaults = [
        'icon'  => 'th',
        'opts' => ['striped','bordered','hover'],
    ];

    public $style = [
        'main' => 'box-info',
        'body' => 'border-radius-none',
    ];

    /************************************************************************/

    protected function populate () {
        return [
            'columns' => $this->data->columns(),
            'rows'    => $this->data->rows(),
        ];
    }
}

