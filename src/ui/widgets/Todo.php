<?php

namespace PHP2USE\ui\widgets;

use PHP2USE\ui\Widget;

/****************************************************************************/

class Todo extends Widget {
    public $value;

    /************************************************************************/

    protected $required = [];
    protected $defaults = [
        'icon'  => 'clipboard',
    ];

    public $style = [
        'main' => 'box-info',
        'body' => '',
    ];

    /************************************************************************/

    protected function populate () {
        return [
            'tasks' => [[
                'type' => 'danger',
                'text' => "Design a nice theme",
                'icon' => 'ellipsis-v',
                'when' => '2 mins',
            ],[
                'type' => 'info',
                'text' => "Make the theme responsive",
                'icon' => 'ellipsis-v',
                'when' => '4 hours ago',
            ],[
                'type' => 'warning',
                'text' => "Let theme shine like star",
                'icon' => 'ellipsis-v',
                'when' => '1 day',
            ]],
        ];
    }
}

