<?php

namespace PHP2USE\ui\widgets;

use PHP2USE\ui\Widget;

/****************************************************************************/

class Calendar extends Widget {
    public $events;

    /************************************************************************/

    protected $required = ['events'];
    protected $defaults = [
        'icon'  => 'calendar',
    ];

    public $style = [
        'main' => 'box-solid bg-green-gradient',
        'body' => '',
    ];

    /************************************************************************/

    protected function populate () {
        return [
            'events' => $this->events,
        ];
    }
}

