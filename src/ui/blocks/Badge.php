<?php

namespace PHP2USE\ui\blocks;

use PHP2USE\ui\Block;

/****************************************************************************/

class Badge extends Block {
    public $link;
    public $icon;
    public $color;

    /************************************************************************/

    protected $required = ['value'];
    protected $defaults = [
        'link'  => null,
        'icon'  => null,
        'color' => null,
    ];

    /************************************************************************/

    protected $size;

    protected function setup () {
        $this->size = ['md' => 3];

        $this->config();
    }

    protected function config () {
        $this->resize('lg', 3)->resize('xs', 6);
    }

    /************************************************************************/

    public function resize ($dim, $cols=3) {
        if (in_array($dim, Block::$bs_cols)) {
            $this->size[$dim] = $cols;
        }

        return $this;
    }

    public function html_class () {
        $resp = [];

        foreach (Block::$bs_cols as $dim) {
            if (array_key_exists($dim, $this->size)) {
                $resp[] = "col-{$dim}-{$this->size[$dim]}";
            }
        }

        return implode(' ', $resp);
    }
}

