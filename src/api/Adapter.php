<?php

namespace PHP2USE\api;

/****************************************************************************/

class Adapter extends Common\Helper {
    private static $registry = [];

    public static function resolve ($parent, $target, $key) {
        if (!array_key_exists($key, Ontology::$registry)) {
            $cls = $parent->rewrite_ns('types', $key);

            $ont = Ontology::resolve($key);

            Ontology::$registry[$key] = new $cls($parent, $target, $ont);
        }

        if (array_key_exists($key, Ontology::$registry)) {
            return Ontology::$registry[$key];
        } else {
            return null;
        }
    }
    
    /********************************************************************/
    
    protected $prn;
    protected $nrw;
    protected $res;
    
    public function __construct ($prn, $nrw, $res) {
        $this->prn = $prn;
        $this->nrw = $nrw;
        $this->res = $res;
        
        $this->initialize();
    }
    
    public function rewrite_ns ($ns) {
        return $this->prn->rewrite_ns($ns);
    }
    
    protected function call() { return call_user_func_array(array($this->prn, 'invoke'), func_get_args()); }
}

