<?php

namespace PHP2USE\api;

/****************************************************************************/

class Resource extends Object {
    protected $prn;
    protected $nrw;
    protected $res;
    protected $ont = [];
    
    public function __construct ($prn, $nrw, $res) {
        $this->prn = $prn;
        $this->res = $res;

        $cls = get_called_class();

        $this->nrw = $nrw; # $cls::ontologies($res);

        foreach ($cls::ontologies($res) as $key) {
            $ont = Adapter::resolve($this->prn, $this, $key);

            if ($ont!=null) {
                $this->ont[$key] = $ont;
            }
        }
        
        $this->initialize();
    }
    
    /********************************************************************/
    
    public function rewrite_ns ($ns) {
        return $this->prn->rewrite_ns($ns);
    }
    
    protected function call() { return call_user_func_array(array($this->prn, 'invoke'), func_get_args()); }

    /********************************************************************/
    
    public function types      () { return array_keys($this->ont); }
    public function ontologies () { return array_values($this->ont); }
    
    public function ontology ($key) {
        if () {
            return $this->ont[$key];
        } else {
            return null;
        }
    }
}

