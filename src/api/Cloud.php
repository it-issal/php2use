<?php

namespace PHP2USE\api;

use PHP2USE\core as Common;
use PHP2USE\cache as Cache;

/****************************************************************************/

class Cloud extends API {
    protected static function rewrite_ns () { return "PHP2USE\\api\\cloud\\".implode('\\', func_get_args()); }

    /***********************************************************************/
    
    protected $prvd;

    protected function prepare ($provider) {
        $this->prvd  = $provider;
    }
    
    /***********************************************************************/
    
    public function provider  () { return $this->prvd; }
    
    public function alias     () { return $this->provider(); }
    public function title     () { return ucfirst($this->provider()); }
    public function icon      () { return $this->provider(); }
    
    /***********************************************************************/
    
    public function supports($key) {
        return in_array($key, $this->features());
    }

    public function features() {
        $mpp = array(
            'google'   => array('oauth','social','media'),
            'facebook' => array('oauth','social','insights','media'),
            'twitter'  => array('oauth','social'),
            'flickr'   => array('oauth','social','media'),
        );
        
        if (array_key_exists($this->name(), $mpp)) {
            return $mpp[$this->name()];
        } else {
            return array();
        }
    }
}

