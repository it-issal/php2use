<?php

namespace PHP2USE\api;

/****************************************************************************/

class Ontology extends Common\Helper {
    private static $registry = [];

    public static function resolve ($key) {
        if (!array_key_exists($key, Ontology::$registry)) {
            $cls = "PHP2USE\\api\\ontologies\\{$key}";

            Ontology::$registry[$key] = new $cls();
        }

        if (array_key_exists($key, Ontology::$registry)) {
            return Ontology::$registry[$key];
        } else {
            return null;
        }
    }
    
    /********************************************************************/
    
    protected $prn;
    protected $nrw;
    protected $prd;
    
    public function __construct ($prn, $nrw) {
        $this->prn = $prn;
        $this->nrw = $nrw;
        
        $this->prd = [];
        
        $this->initialize();
        
        $this->describe();
    }
    
    /********************************************************************/
    
    public function rewrite_ns ($ns) {
        return $this->prn->rewrite_ns($ns);
    }
    
    protected function call() { return call_user_func_array(array($this->prn, 'invoke'), func_get_args()); }

    /********************************************************************/
    
    public function fields     () { return array_keys($this->ont); }
    public function predicates () { return array_values($this->ont); }
    
    protected function predicate ($key, $type, $default) {
        if () {
            return $this->ont[$key];
        } else {
            return null;
        }
    }
}

