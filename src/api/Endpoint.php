<?php

namespace PHP2USE\api;

use PHP2USE\core as Common;
use PHP2USE\cache as Cache;

/****************************************************************************/

class Endpoint extends API {
    protected static function rewrite_ns () { return "PHP2USE\\api\\endpoints\\".implode('\\', func_get_args()); }

    /***********************************************************************/
    
    protected $prvd;
    protected $name;

    protected function prepare ($provider, $alias) {
        $this->prvd  = $provider;
        $this->name  = $alias;
    }

    /***********************************************************************/

    public function provider  () { return $this->prvd; }
    public function name      () { return $this->name; }
    public function icon      () { return $this->icon; }
    
    public function alias     () { return "{$this->prvd} :: {$this->name}"; }
    public function title     () { return ucfirst($this->prvd).' :: '.ucfirst($this->name); }
    
    /***********************************************************************/
    
    public function supports($key) {
        switch ($key) {
            case 'oauth':
                break;

            case 'insights':
                break;
        }

        return in_array($key, $this->features());
    }

    public function features() {
        $mpp = array(
            'google'   => array('oauth','social','media'),
            'facebook' => array('oauth','social','insights','media'),
            'twitter'  => array('oauth','social'),
            'flickr'   => array('oauth','social','media'),
        );
        
        if (array_key_exists($this->name(), $mpp)) {
            return $mpp[$this->name()];
        } else {
            return array();
        }
    }
}

