<?php

namespace PHP2USE\api\endpoints\sparql;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

use PHP2USE\api\Endpoint;
use PHP2USE\api\Resource;

/****************************************************************************/

class Bridge extends Endpoint {
    protected $icon = 'cubes';

    /***************************************************************************************************/
    
    public function html5_header () {
?>
<?php
    }
    public function html5_footer () {
?>
<?php
    }
    
    /************************************************************************/
    
    public function initialize ($creds, $cfg, $vault) {
        if (!array_key_exists('namespaces', $cfg)) {
            $cfg['namespaces'] = [
                'category' => 'http://dbpedia.org/resource/Category:',
                'dbpedia'  => 'http://dbpedia.org/resource/',
                'dbo'      => 'http://dbpedia.org/ontology/',
                'dbp'      => 'http://dbpedia.org/property/',
            ];
        }

        foreach ($cfg['namespaces'] as $alias => $link) {
            // EasyRdf_Namespace::set($alias, $link);
        }
    }

    public function connect ($creds, $cfg, $vault) {
        return null;

        if (!array_key_exists('target_link', $cfg)) {
            $cfg['target_link'] = 'http://dbpedia.org/sparql';
        }

        $cnx = new EasyRdf_Sparql_Client($cfg['target_link']);

        return $cnx;
    }

    /************************************************************************/
    
    public function query ($stmt='') {
        if (strlen($stmt)==0) {
            $stmt = 'SELECT * WHERE {'.
                    '  ?country rdf:type dbo:Country .'.
                    '  ?country rdfs:label ?label .'.
                    '  ?country dc:subject category:Member_states_of_the_United_Nations .'.
                    '  FILTER ( lang(?label) = "en" )'.
                    '} ORDER BY ?label';
        }

        $result = $sparql->query($stmt);

        foreach ($result as $row) {
            #echo "<li>".link_to($row->label, $row->country)."</li>\n";
        }

        #echo $result->numRows();

        return $result;
    }
}

