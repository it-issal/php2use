<?php

namespace PHP2USE\api\endpoints\piwik;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

use PHP2USE\api\Endpoint;
use PHP2USE\api\Resource;

/****************************************************************************/

class Bridge extends Endpoint {
    protected $icon = 'stats-o';

    /***************************************************************************************************/
    
    public function html5_header () {
?>
<?php
    }
    public function html5_footer () {
?>
<?php
        }
    }
    
    /***************************************************************************************************/
    
    public function initialize ($creds, $cfg, $vault) {
        
    }
    public function connect ($creds, $cfg, $vault) {
        
    }
}

