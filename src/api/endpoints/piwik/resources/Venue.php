<?php

namespace PHP2USE\api\endpoints\foursquare\resources;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

use PHP2USE\api\API;
use PHP2USE\api\Resource;

/****************************************************************************/

class Venue extends Resource {
    public static function narrow($obj) {
        return $obj->id;
    }
    
    protected function initialize() {
        
    }
    
    public function uid ()        { return $this->res->id; }
    public function link ()       { return $this->res->canonicalUrl; }
    
    public function likes ()      { return $this->res->likes->count; }
    public function checkins ()   { return $this->res->stats->checkinsCount; }
    public function users ()      { return $this->res->stats->usersCount; }
    public function tips ()       { return $this->res->stats->tipCount; }
    public function hereNow ()    { return $this->res->hereNow; }
    
    public function categories () { return $this->res->categories; }
    public function contact ()    { return $this->res->contact; }
    public function location ()   { return $this->res->location; }
    public function openHours ()  { return $this->res->hours; }
}

