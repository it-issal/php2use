<?php

namespace PHP2USE\api\endpoints\odoo;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

use PHP2USE\api\Endpoint;
use PHP2USE\api\Resource;

/****************************************************************************/

class Bridge extends Endpoint {
    protected $icon = 'bank';

    /***************************************************************************************************/
    
    public function html5_header () {
?>
<?php
    }
    public function html5_footer () {
?>
<?php
    }
    
    /***************************************************************************************************/
    
    protected function initialize ($creds, $cfg, $vault) {
        
    }
    protected function connect ($creds, $cfg, $vault) {
        $cnx = new \Jsg\Odoo\Odoo($cfg['endpoint'], $cfg['database'], $creds['username'], $creds['password']);

        return $cnx;
    }
}

