<?php

namespace PHP2USE\api\endpoints\wordpress;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

use PHP2USE\api\Endpoint;
use PHP2USE\api\Resource;

use HieuLe\WordpressXmlrpcClient\WordpressClient;
use HieuLe\WordpressXmlrpcClient\Exception\XmlrpcException;

/****************************************************************************/

class Bridge extends Endpoint {
    protected $icon = 'wordpress';

    /***************************************************************************************************/

    public function html5_header () {
?>
<?php
    }
    public function html5_footer () {
?>
<?php
    }
    
    /***************************************************************************************************/
    
    private $log;

    public function initialize ($creds, $cfg, $vault) {

    }
    public function connect ($creds, $cfg, $vault) {
        $log = new \Monolog\Logger('wp-xmlrpc');

        $cnx = new WordpressClient();

        $cnx->onError(function($error, $event) use ($log) {
            $log->addError($error, $event);
        });

        $cnx->setCredentials($cfg['endpoint'], $creds['username'], $creds['password']);

        return $cnx;
    }
    
    /***************************************************************************************************/
    
    public function users () {
        try {
            return $this->cursor()->getUsers();
        } catch (XmlrpcException $ex) {
            return [];
        }
    }
    
    /***************************************************************************************************/
    
    public function types () {
        try {
            $resp = [];

            foreach ($this->cursor()->getPostTypes() as $type) {
                $resp[] = $type['name'];
            }

            return $resp;
        } catch (XmlrpcException $ex) {
            return [];
        }
    }
    
    /***************************************************************************************************/
    
    public function all () {
        try {
            $resp = [];

            $lst = null;

            foreach ($this->types() as $type) {
                try {
                    $lst = $this->cursor()->getPosts([
                        'post_type' => $type,
                    ]);
                } catch (XmlrpcException $ex) {
                    $lst = null;
                }

                if (is_array($lst)) {
                    $resp = array_merge($resp, $lst);
                }
            }

            return $resp;
        } catch (XmlrpcException $ex) {
            return [];
        }
    }
    
    /***************************************************************************************************/

    /*
    public function posts () {
        $resp = $this->cursor()->getPosts();

        return $resp;
    }
    //*/
}

