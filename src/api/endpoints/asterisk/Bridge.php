<?php

namespace PHP2USE\api\endpoints\asterisk;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

use PHP2USE\api\Endpoint;
use PHP2USE\api\Resource;

/****************************************************************************/

use PAGI\CallSpool\CallFile;
use PAGI\CallSpool\Impl\CallSpoolImpl;
use PAGI\DialDescriptor\SIPDialDescriptor;
use PAGI\DialDescriptor\DAHDIDialDescriptor;

\PAGI\Autoloader\Autoloader::register(); // Call autoloader register for PAGI autoloader.

/****************************************************************************/

class Bridge extends Endpoint {
    protected $icon = 'telephone';

    public function html5_header () {
?>
<?php
    }
    public function html5_footer () {
?>
<?php
    }
    
    /************************************************************************/
    
    public function initialize ($creds, $cfg, $vault) {
        
    }

    public function connect ($creds, $cfg, $vault) {
        
    }
    
    /************************************************************************/

    public function dahdi ($extension, $channel) {
        $resp = new DAHDIDialDescriptor('1949890333', 1);

        #Application::curr()->endpoint('asterisk','srv-cloud')->dahdi('699144720')->channels();
        
        return $resp;
    }
    
    /************************************************************************/
    
    public function call () {
        $dialDescriptor = new DAHDIDialDescriptor('1949890333', 1);

        $callFile = new CallFile($dialDescriptor);
        $callFile->setContext('campaign');
        $callFile->setExtension('failed');
        $callFile->setVariable('foo', 'bar');
        $callFile->setPriority('1');
        $callFile->setMaxRetries('0');
        $callFile->setWaitTime(10);
        $callFile->setCallerId('some<123123>');

        echo "Call file generated (DAHDI dial descriptor):\n";
        echo $callFile->serialize();
        echo "\n\n";

        $dialDescriptor = new SIPDialDescriptor('24', 'example.com');

        $callFile = new CallFile($dialDescriptor);
        $callFile->setContext('default');
        $callFile->setExtension('777');
        $callFile->setVariable('foo', 'bar');
        $callFile->setPriority('1');
        $callFile->setMaxRetries('0');
        $callFile->setWaitTime(10);
        $callFile->setCallerId('some<123123>');

        echo "Call file generated (SIP dial descriptor):\n";
        echo $callFile->serialize();
        echo "\n\n\n";

        echo "Spooling generated SIP call\n";
        $spool = CallSpoolImpl::getInstance(
            array(
                'tmpDir' => '/tmp',
                'spoolDir' => '/tmp/spoolExample'
            )
        );

        $spool->spool($callFile);

        echo "Spooling generated SIP call to run in 30 seconds\n";
        $spool->spool($callFile, time() + 30);
    }
}

