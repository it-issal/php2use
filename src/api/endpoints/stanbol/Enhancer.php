<?php

require("bootstrap.php");

$configuration = array(); // Advanced Guzzle configuration

$stanbolClient = new \Stanbol\Client\StanbolClient::getInstance(STANBOL_ENDPOINT, $configuration);
$enhancements = $stanbolClient->enhancer()->enhance("Paris is the capital of France");

foreach($enhancements->getTextAnnotations() as $textAnnotation) {
    echo "********************************************\n";
    echo "Selection Context: ".$textAnnotation->getSelectionContext()."\n";  
    echo "Selected Text: ".$textAnnotation->getSelectedText()."\n";
    echo "Engine: ".$textAnnotation->getCreator()."\n";
    echo "Candidates:\n";
    foreach($enhancements->getEntityAnnotations($textAnnotation) as $entityAnnotation) {
          echo "\t" . $entityAnnotation->getEntityLabel() . " - " . $entityAnnotation->getEntityReference()->getUri() . " - " .$entityAnnotation->getConfidence() ."\n";
    }
}
