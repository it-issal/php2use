<?php

$ldPathProgram = 
    "@prefix find:<http://stanbol.apache.org/ontology/entityhub/find/>; 
     find:labels = rdfs:label[@en] :: xsd:string; 
     find:comment = rdfs:comment[@en] :: xsd:string; 
     find:categories = dc:subject :: xsd:anyURI; 
     find:mainType = rdf:type :: xsd:anyURI;";

// Search in DBPedia Referenced Site
$entityhubService->setSite('dbpedia');
$program = new LDPathProgram(ldPathProgram);
$entities = $entityhubService->find("Par*", null, "en", $program, 10, 0); // Return the list of entities
