<?php

// Create
$entityhubService = $stanbolClient->entityhub();
$entityhubService->setSite(SITE); // If you want to add the entity to a custom site instead of memory site
$entity = new \Stanbol\Vocabulary\Model\Entity(ENTITY_URI);
$entity->addPropertyValue($property, $value, $lang);
$entityhubService->create($entity); // Return the entity id if the entity was created successfully or throw an exception if the entity could not be created or a connection error occurred

// Create from file
$entityhubService->createFromFile(FILE); // Return the entity id if the entity was created successfully or throw an exception if an error occurred

// Retrieve
$entity = $entityhubService->get(RESOURCE_ID); // Return the entity if exists or throw an exception if an error ocurred

// Delete
$deleted = $entityhubService->delete(RESOURCE_ID); // Return a boolean indicating whether the entity has been successfully deleted or not

