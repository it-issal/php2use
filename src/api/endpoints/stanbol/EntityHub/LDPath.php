<?php

$ldPathProgram = 
    "@prefix find:<http://stanbol.apache.org/ontology/entityhub/find/>; 
     find:labels = rdfs:label[@en] :: xsd:string; 
     find:comment = rdfs:comment[@en] :: xsd:string; 
     find:categories = dc:subject :: xsd:anyURI; 
     find:mainType = rdf:type :: xsd:anyURI;";

// Use DBPedia Referenced Site
$entityhubService->setSite('dbpedia');
$program = new \Stanbol\Services\Entityhub\Model\LDPathProgram($ldPathProgram);
$properties = $entityhubService->ldpath("http://dbpedia.org/resource/Paris", $ldpathProgram); // Return the associative array with properties and values. The resulting properties are mapped using the supplied LDPath program

