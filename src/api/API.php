<?php

namespace PHP2USE\api;

use PHP2USE\core as Common;
use PHP2USE\cache as Cache;

/****************************************************************************/

class API extends Common\Helper {
    public static function instance () {
        $args = func_get_args();

        $prnt = get_called_class();

        $cls = $prnt::rewrite_ns($args[0][0], 'Bridge');

        $rst = new $cls($args[0], $args[1], $args[2], $args[3]);

        return $rst;
    }

    /***********************************************************************/

    private function __construct ($narrow, $creds, $cfg=array(), $vault=array()) {
        call_user_func_array([$this,'prepare'], $narrow);

        $this->creds = $creds;
        $this->cfg   = $cfg;
        
        $this->vault = []; #new Cache\Wrapper("apis://vault@%s/%s", $vault, array($this->name));
        $this->cache = []; #new Cache\Wrapper("apis://cache:%s@%s/%s?%s");

        call_user_func_array([$this,'initialize'], [
            $this->creds,
            $this->cfg,
            $this->vault,
            #$this->cache,
        ]);

        $this->cnx = null;
    }
    
    /***********************************************************************/
    
    protected $creds;
    protected $cfg;
    protected $vault;
    
    protected $cache;
    
    /***********************************************************************/
    
    public function classname () { $cls = get_called_class(); return $cls::rewrite_ns($this->name); }
    
    /***********************************************************************/

    protected $cnx;
    
    public function cursor() {
        if ($this->cnx==null) {
            $this->cnx = $this->connect( $this->creds, $this->cfg, $this->vault );
        }

        return $this->cnx;
    }
    
    /***********************************************************************/
    
    public function reflect($ns, $resp, $callback) {
        return $this->wrap($ns, $resp, $callback);
    }
    
    public function invoke($verb, $link, $args=array()) {
        $nrw = array($verb, $this->name, $link, http_build_query($args));
        
        if ($this->cache->has($nrw)) {
            return $this->cache->get($nrw);
        } else {
            $resp = $this->call($verb, $link, $args);
            
            if ($resp!=null) {
                return $this->cache->set($nrw, $resp);
            } else {
                return null;
            }
        }
    }
    
    /***********************************************************************/
    
    protected function call_http ($verb, $url, $payload=array()) {
        $chandle = curl_init();
        curl_setopt($chandle, CURLOPT_URL, $url);
        curl_setopt($chandle, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chandle);
        curl_close($chandle);
        
        return $result;
    }
    protected function call_json ($verb, $url, $payload=array()) {
        $resp = $this->call_http($verb, $url, $payload);
        
        $data = json_decode($resp);
        
        return $data;
    }
    protected function call_xml ($verb, $url, $payload=array()) {
        $resp = $this->call_http($verb, $url, $payload);
        
        $data = new \SimpleXMLElement(html_entity_decode($resp));
        
        return $data;
    }
}

