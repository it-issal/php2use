<?php

namespace PHP2USE\api\cloud\linkedin;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

use PHP2USE\api\API;
use PHP2USE\api\Resource;

/****************************************************************************/

class Bridge extends API {
    public function html5_header () {
        if (array_key_exists('user_id', $this->cfg)) {
?>
        <link rel="author" href="https://plus.google.com/<?php echo $this->cfg['user_id'] ?>"/>
        <link rel="publisher" href="https://plus.google.com/<?php echo $this->cfg['user_id'] ?>"/>
<?php
		}
		
        if (Site::has('stats.webmaster-tools')) {
?>
        <meta name="google-site-verification" content="<?php echo Site::get("stats.webmaster-tools") ?>" />
<?php
        }
    }
    public function html5_footer () {
        if (Site::has('stats.analytics')) {
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php echo Site::get("stats.analytics") ?>', '<?php echo Site::get("site.domain") ?>');
  ga('send', 'pageview');
</script>
<?php
        }
    }
    
    /***************************************************************************************************/
    
    public function hybrid_name () { return 'Google'; }
    public function hybrid_strategy () {
        return array (
            "enabled"         => true,
            "keys"            => array(
                "id" => $this->creds['api_key'],
                "secret" => $this->creds['api_pki'],
            ),
            "scope"           => implode(' ', array(
                'https://www.googleapis.com/auth/userinfo.profile',
                'https://www.googleapis.com/auth/userinfo.email',
            )),
            "access_type"     => "offline", // optional
            "approval_prompt" => "force", // optional
            "hd"              => Site::get('site.domain'), // optional
        );
    }
    
    /***************************************************************************************************/
    
    public function initialize () {
        
    }
    public function connect ($creds, $cfg, $vault) {
        
    }
    
    /***************************************************************************************************/
    
    protected function call ($uri, $args=array()) {
        $url  = "https://graph.facebook.com/{$uri}?access_token={$this->vault['access_token']}&{$args}";
        
        $resp = $this->call_json('GET', $url);
        
        return $resp;
    }
}

