<?php

namespace PHP2USE\api\cloud\bitbucket;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

use PHP2USE\api\API;
use PHP2USE\api\Resource;

/****************************************************************************/

class Bridge extends API {
    protected $icon = 'bitbucket-o';

    public function html5_header () {
        // <meta property="og:locale" content="fr_fr" />
?>
<?php
    }
    public function html5_footer () {
?>
        
<?php
    }
    
    /***************************************************************************************************/
    
    public function initialize () {
        //Site::load_foss('facebook4php', 'src/facebook');
    }
    public function connect ($creds, $cfg, $vault) {
        $cnx = new Bitbucket\API\Authentication\Basic($creds['username'], $creds['password']);

        return $cnx;
    }
    
    /***************************************************************************************************/
    
    protected function repos () {
        $lst = new Bitbucket\API\Repositories();

        $lst->setCredentials($this->cnx);

        return $lst;
    }
    
    public function all_repos  ()      { return $this->repos()->all(); }
    public function user_repos ($user) { return $this->repos()->all($user); }
    
    /***************************************************************************************************/
    
    public function user($uid=null) {
        if ($uid==null) {
            $uid = $this->cfg['venue_id'];
        }
        
        $resp = $this->invoke('GET', "v2/venues/{$uid}");
        
        return $this->reflect('Repository', $resp, function ($obj) {
            return $obj->response->venue;
        });
    }
}

