<?php

namespace PHP2USE\api\cloud\foursquare;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

use PHP2USE\api\API;
use PHP2USE\api\Resource;

/****************************************************************************/

class Bridge extends API {
    public function html5_header () {
        // <meta property="og:locale" content="fr_fr" />
?>
<?php
    }
    public function html5_footer () {
?>
        
<?php
    }
    
    /***************************************************************************************************/
    
    public function initialize () {
        //Site::load_foss('facebook4php', 'src/facebook');
    }
    public function connect ($creds, $cfg, $vault) {
        $client = new \Github\HttpClient\CachedHttpClient();

        $client->setCache(
            new \Github\HttpClient\Cache\FilesystemCache('/tmp/github-api-cache')
        );

        $repositories = $client->api('user')->repositories('ornicar');
    }
    
    /***************************************************************************************************/
    
    public function venue($uid=null) {
        if ($uid==null) {
            $uid = $this->cfg['venue_id'];
        }
        
        $resp = $this->invoke('GET', "v2/venues/{$uid}");
        
        return $this->reflect('FoursquareVenue', $resp, function ($obj) {
            return $obj->response->venue;
        });
    }
}

