<?php

namespace PHP2USE\api\cloud\flickr\resources;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

use PHP2USE\api\API;
use PHP2USE\api\Resource;

/****************************************************************************/

class Photoset extends Resource {
    public static function narrow($obj) {
        return $obj->id;
    }
    
    private $cover=null;
    
    protected function initialize() {
        $this->cover = null;
        //$this->pics = $this->remap($this->res->photo);
    }
    
    public function uid ()     { return $this->res->id; }
    public function title ()   { return $this->res->title->_content; }
    public function summary () { return $this->res->summary; }
    
    public function cover () {
        if ($this->cover==null) {
            $resp = $this->photos();
            
            if (sizeof($resp)) {
                $this->cover = $resp[0];
            }
        }
        
        return $this->cover;
    }
    
    public function photos () {
        $resp = $this->prn->invoke('GET', 'photosets.getPhotos', array(
            'photoset_id' => $this->uid(),
            'extras'      => implode(',',Photo::$FIELDs),
        ));
        
        // Site::halt(500, print_r($this->uid(), true));
        
        return $this->remap('Photo', $resp, function ($obj) {
            return $obj->photoset->photo;
        });
    }
}

