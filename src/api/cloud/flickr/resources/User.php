<?php

namespace PHP2USE\api\cloud\flickr\resources;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

use PHP2USE\api\API;
use PHP2USE\api\Resource;

/****************************************************************************/

class User extends Resource {
    public static function narrow($obj) {
        return $obj->nsid;
    }
    
    protected function initialize() {
        
    }
    
    public function uid ()     { return $this->res->id; }
    
    public function photosets() {
        return $this->prn->getSets($this->nrw);
    }
}

