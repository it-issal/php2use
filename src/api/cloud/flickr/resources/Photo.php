<?php

namespace PHP2USE\api\cloud\flickr\resources;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

use PHP2USE\api\API;
use PHP2USE\api\Resource;

/****************************************************************************/

class Photo extends Resource {
    public static $FIELDs = array('license','date_upload','date_taken','owner_name','icon_server','original_format','last_update','geo','tags','machine_tags','o_dims','views','media','path_alias','url_sq','url_t','url_s','url_m','url_o');
    
    public static function narrow($obj) {
        return $obj->id;
    }
    
    protected function initialize() {
        
    }
    
    public function uid ()     { return $this->res->id; }
    public function title ()   { return $this->res->title; }
    
    public function link() {
        return "http://farm{$this->res->farm}.staticflickr.com/{$this->res->server}/{$this->res->id}_{$this->res->secret}_b.jpg";
    }
}

