<?php

namespace PHP2USE\api\cloud\flickr;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

use PHP2USE\api\API;
use PHP2USE\api\Resource;

/****************************************************************************/

class Bridge extends SocialAPI {
    public function html5_header () {
?>
        
<?php
    }
    public function html5_footer () {
?>
        
<?php
    }
    
    /***************************************************************************************************/
    
    public function opauth_strategy () {
        return array(
            'Flickr' => array(
                'app_id'     => $this->creds['api_key'],
                'app_secret' => $this->creds['api_pki'],
            ),
        );
    }
    
    /***************************************************************************************************/
    
    public function initialize () {
        
    }
    public function connect ($creds, $cfg, $vault) {
        
    }
    
    /***************************************************************************************************/
    
    public function call ($verb, $method, $args=array()) {
        $args['method']         = "flickr.{$method}";
        $args['api_key']        = $this->creds['api_key'];
        $args['format']         = 'json';
        $args['nojsoncallback'] = '1';
        
        $resp = $this->call_json($verb, "https://api.flickr.com/services/rest/?".http_build_query($args));
        
        return $resp;
        if ($resp && $resp->meta) {
            switch (intval($resp->meta->code)) {
                case 200:
                    return $resp;
                    break;
                default:
                    throw new ReflectionException($this, $verb, $method, $args, $resp);
                    break;
            }
        } else {
            print_r($resp); die(1);
        }
        
        return null;
    }
    
    /***************************************************************************************************/
    
    public function user ($uid=null) {
        if ($uid==null) {
            $uid = $this->cfg['user_id'];
        }
        
        $resp = $this->invoke('GET', 'people.getInfo', array('user_id' => $uid));
        
        return $this->reflect('User', $resp, function ($obj) {
            return $obj->person;
        });
    }
    public function getSets ($uid=null) {
        if ($uid==null) {
            $uid = $this->cfg['user_id'];
        }
        
        $resp = $this->invoke('GET', 'photosets.getList', array('user_id' => $uid));
        
        return $this->remap('Photoset', $resp, function ($obj) {
            return $obj->photosets->photoset;
        }, function ($obj) {
            return $obj;
        });
    }
    public function getPhotoSet ($sid) {
        return $this->reflect('Photoset',
            $this->invoke('GET', 'photosets.getPhotos', array(
                'photoset_id' => $sid,
            )),
            function ($obj) {
                return $obj->photoset;
            }
        );
    }
}

