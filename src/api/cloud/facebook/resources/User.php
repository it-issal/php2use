<?php

namespace PHP2USE\api\cloud\facebook\resources;

use PHP2USE\Reactor;
use PHP2USE\web\Application;

use PHP2USE\api\API;
use PHP2USE\api\Resource;

/****************************************************************************/

class User extends Resource {
    public static function narrow($obj) {
        return $obj->id;
    }
    
    protected function initialize() {
        
    }
    
    public function uid ()          { return $this->res->id; }

    public function name ()         { return $this->res->name; }
    public function about ()        { return $this->res->about; }
    public function summary ()      { return $this->res->descrition; }
    
    public function category ()     { return $this->res->category; }
    public function phone ()        { return $this->res->phone; }
    public function address ()      { return $this->res->location; }
    public function full_address () { return "{$this->address->street}\n{$this->address->zip}\n{$this->address->city}, {$this->address->country}"; }
    
    public function link ()         { return $this->res->link; }
    public function website ()      { return $this->res->website; }
    public function location ()     { return $this->res->location; }
    
    public function likes ()        { return $this->res->likes; }
    public function checkins ()     { return $this->res->checkins; }
    public function talkingAbout () { return $this->res->talking_about_count; }
    public function hereNow ()      { return $this->res->were_here_count; }
    
    public function categories ()   { return $this->res->categories; }
    public function contact ()      { return $this->res->contact; }
    public function openHours ()    { return $this->res->hours; }
}

