<?php

namespace PHP2USE\api\cloud\facebook;

use PHP2USE\Site;
use PHP2USE\APIs\SocialAPI;
use PHP2USE\APIs\SocialResource;
use PHP2USE\Common as Common;

/****************************************************************************/

class Bridge extends API {
    public function html5_header () {
        $page = $this->page();
?>
    <meta property="og:locale" content="fr_fr" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo $page->name() ?>" />
    <meta property="og:url" content="<?php echo $page->link() ?>" />
    <meta property="og:site_name" content="<?php echo $page->website() ?>" />
<?php
    }
    public function html5_footer () {
?>
        <script>
window.fbAsyncInit = function() {
    FB.init({
        appId      : "<?php echo $this->creds['api_key']?>",
        xfbml      : true,
        version    : 'v2.1'
    });
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
        </script>
<?php
    }
    
    /***************************************************************************************************/
    
    public function hybrid_name () { return 'Facebook'; }
    public function hybrid_strategy () {
        return array (
            "enabled"         => true,
            "keys"            => array(
                "id"     => $this->creds['api_key'],
                "secret" => $this->creds['api_pki'],
            ),
            'scope'           => implode(', ', array(
                'email', 'user_about_me', 'user_birthday', 'user_hometown',
            )),
            'display' => 'popup', // optional
        );
    }
    
    /***************************************************************************************************/
    
    public function initialize () {
        // Site::load_foss('facebook4php', 'src/facebook');
    }
    public function connect ($creds, $cfg, $vault) {
        
    }
    
    /***************************************************************************************************/
    
    protected function call ($verb, $link, $args=array()) {
        if (array_key_exists('access_token', $this->vault)) {
            $args['access_token'] = $this->vault['access_token'];
        }
        
        $url = "https://graph.facebook.com/{$link}?".http_build_query($args);
        
        $resp = $this->call_json($verb, $url);
        
        //print_r($resp); //die(1);
        
        return $resp;
    }
    
    /***************************************************************************************************/
    
    public function page($uid=null) {
        if ($uid==null) {
            $uid = $this->cfg['page_id'];
        }
        
        $resp = $this->invoke('GET', "{$uid}");
        
        //print_r($resp); //die(1);
        
        return $this->reflect('FacebookPage', $resp, function ($obj) {
            return $obj;
        });
    }
}

