<?php

namespace PHP2USE\api;

/****************************************************************************/

class Activity extends Ontology {
    protected function describe () {
        $this->predicate('when', 'string');
        $this->predicate('who',  'string');
        $this->predicate('what', 'string');
        $this->predicate('how',  'string');
    }
}

