<?php

namespace PHP2USE\api;

/****************************************************************************/

class Contact extends Ontology {
    protected function describe () {
        $this->predicate('first_name', 'string');
        $this->predicate('last_name',  'string');
    }
}

