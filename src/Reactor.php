<?php

namespace PHP2USE;

use PHP2USE\core\Observable;

use PHP2USE\web\Application;

use Symfony\Component\Yaml\Yaml;

/****************************************************************************/

class Reactor extends Observable {
    public static function log      () { return Application::reflect('curr', 'log',      func_get_args()); }

    /***********************************************************************/

    public static function request  () { return Application::reflect('curr', 'request',  func_get_args()); }
    public static function response () { return Application::reflect('curr', 'response', func_get_args()); }

    public static function POST     () { return Application::reflect('curr', 'POST',     func_get_args()); }
    public static function GET      () { return Application::reflect('curr', 'GET',      func_get_args()); }
    public static function PUT      () { return Application::reflect('curr', 'POST',     func_get_args()); }
    public static function DELETE   () { return Application::reflect('curr', 'DELETE',   func_get_args()); }
    public static function OPTIONS  () { return Application::reflect('curr', 'OPTIONS',  func_get_args()); }
    
    /***********************************************************************/

    public static function format   () { return Application::reflect('curr', 'format',   func_get_args()); }
    public static function respond  () { return Application::reflect('curr', 'respond',  func_get_args()); }
    public static function render   () { return Application::reflect('curr', 'render',   func_get_args()); }

    public static function redirect () { return Application::reflect('curr', 'redirect', func_get_args()); }
    public static function gohome   () { return Application::reflect('curr', 'gohome',   func_get_args()); }

    public static function flash    () { return Application::reflect('curr', 'flash',    func_get_args()); }
    public static function flashNow () { return Application::reflect('curr', 'flash',    func_get_args()); }
    
    /***********************************************************************/
    
    public static function bpath () {
        return APP_ROOT;
    }

    /***********************************************************************/

    public static function load_fs   () {return file_get_contents(call_user_func_array(['\PHP2USE\Reactor','rpath'], func_get_args())); }
    public static function load_json () {return json_decode(call_user_func_array(['\PHP2USE\Reactor','load_fs'], func_get_args())); }
    public static function load_yaml () {return Yaml::parse(call_user_func_array(['\PHP2USE\Reactor','load_fs'], func_get_args())); }

    public static function is_dir    () {return is_dir(call_user_func_array(['\PHP2USE\Reactor','rpath'], func_get_args())); }
}

