<?php

namespace PHP2USE\ext\hive\ODM;

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;

use Doctrine\ORM\EntityManager;

use Doctrine\MongoDB\Connection;

use Doctrine\ODM\MongoDB\Configuration;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;

/****************************************************************************/

class Provider extends DataProvider {
    public static function repo($narrow) {
        return ORM::$mgr->getRepository($narrow);
    }

    /***********************************************************************/

    public static function console () {
        return ConsoleRunner::createHelperSet(ORM::$mgr);
    }

    /***********************************************************************/

    protected function before () {
        if (false) {
            ORM::$cfg = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src"), $isDevMode);
            // or if you prefer yaml or XML
            //$config = Setup::createXMLMetadataConfiguration(array(__DIR__."/config/xml"), $isDevMode);
            //$config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/config/yaml"), $isDevMode);

            ORM::$cnx = array(
                'driver' => 'pdo_sqlite',
                'path'   => __DIR__ . '/db.sqlite',
            );

            ORM::$mgr = EntityManager::create(ORM::$cnx, ORM::$cfg);
        } else {
            ORM::$conn = new Connection();

            ORM::$cfg = new Configuration();
            ORM::$cfg->setProxyDir(Reactor::rpath('cache', 'doctrine', 'proxies'));
            ORM::$cfg->setProxyNamespace('Proxies');
            ORM::$cfg->setHydratorDir(Reactor::rpath('cache', 'doctrine', 'hydrators'));
            ORM::$cfg->setHydratorNamespace('Hydrators');
            ORM::$cfg->setDefaultDB('doctrine_odm');
            ORM::$cfg->setMetadataDriverImpl(AnnotationDriver::create(Reactor::rpath('models')));

            AnnotationDriver::registerAnnotationClasses();

            ORM::$mgr = DocumentManager::create(ORM::$conn, ORM::$cfg);
        }

        /*
        $conn1 = $container->getService('doctrine.odm.mongodb.conn1_connection');
        $conn2 = $container->getService('doctrine.odm.mongodb.conn2_connection');

        $dm1 = $container->getService('doctrine.odm.mongodb.dm1_document_manager');
        $dm2 = $container->getService('doctrine.odm.mongodb.dm2_document_manager');
        //*/
    }

    /***********************************************************************/

    protected function after () {
        
    }
}

