<?php

namespace PHP2USE\data\backends\graph;

/****************************************************************************/

class Traversal extends \PHP2USE\data\Lookup { # A cypher query
    private $grp;

    private $rst;
    private $tab;

    public function __construct ($graph, $statement, $params) {
        parent::__construct([]);

        $this->grp  = $graph;

        $this->stmt = $statement;
        $this->args = $params;

        $this->nodes = [];
        $this->edges = [];

        $this->reset();
    }

    public function graph     () { return $this->grp; }
    public function statement () { return $this->stmt; }
    public function args      () { return $this->args; }

    public function query     () { return $this->query; }
    public function payload   () { return $this->payload; }

    public function cursor    () { return $this->graph()->cursor(); }

    public function reset () {
        $this->query   = $this->cursor()->sendCypherQuery($this->stmt, $this->args);

        $this->rst = $this->query->getResult();
        $this->tab = $this->query->getRows();

        $this->tab = array_values($this->tab);

        if (sizeof($this->tab)==1) {
            $this->tab = $this->tab[0];
        }

        $this->nodes = [];
        $this->edges = [];

        $positions = [];
        $i = 0;

        foreach ($this->rst->getNodes() as $node){
            $prop = ($node->getLabel() === 'Movie') ? 'title' : 'name';
            $this->nodes[] = [
                #'title' => $node->getProperty($prop),
                'label' => $node->getLabel()
            ];
            $positions[$node->getId()] = $i;
            $i++;
        }

        foreach ($this->rst->getRelationships() as $rel){
            $this->edges[] = [
                'source' => $positions[$rel->getStartNode()->getId()],
                'target' => $positions[$rel->getEndNode()->getId()]
            ];
        }

        $this->nodes = [];
        $this->edges = [];

        foreach ($this->rst->getNodes() as $node){
            $prop = ($node->getLabel() === 'Movie') ? 'title' : 'name';

            $this->nodes[$node->getId()] = [
                #'title' => $node->getProperty($prop),
                'label' => $node->getLabel()
            ];
        }

        foreach ($this->rst->getRelationships() as $rel){
            $this->edges[] = [
                'source' => $this->nodes[$rel->getStartNode()->getId()],
                'target' => $this->nodes[$rel->getEndNode()->getId()]
            ];
        }
    }

    public function nodes     () { return $this->rst->getNodes(); }
    public function edges     () { return $this->rst->getRelationships(); }

    public function columns () {
        if (0<sizeof($this->tab)) {
            return array_keys($this->tab[0]);
        } else {
            return array_keys($this->tab);
        }
        return ['Engine','Browser','Platforms','Version','CSS grade'];
    }
    public function rows () {
        return $this->tab;
    }
}

