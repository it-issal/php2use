<?php

namespace PHP2USE\data\backends\graph;

/****************************************************************************/

class Node extends \PHP2USE\data\Resource {
    private $graph;
    private $nrw;
    private $map;

    public function __construct ($graph, $narrow) {
        $this->graph = GraphDB::instance($graph);
        $this->nrw   = $nrw;
        $this->map   = $this->query('team', "team.narrow='{$this->narrow()}'", [
            'team' => Team::$ontology,
        ]);

        if (sizeof($this->map->nodes())==0) {
            Reactor::halt(404, "Team '{$nrw}' not found !");
        }
    }

    public function graph    () { return $this->graph; }
    public function mapper   () { return $this->map; }

    public function ontology () { return Team::$ontology; }
    public function alias    () { return Team::$alias; }
    public function narrow   () { return $this->nrw; }

    public function node     () { return $this->map->nodes()[0]; }
    public function edges    () { return $this->map->edges(); }

    public function query ($stmt, $params=[]) {
        return new Traversal($this, $stmt, $params);
    }

    public function traverse ($match="n", $where="", $ontologies=[]) {
        $args = [];

        $ontologies = [
            Team::$alias => Team::$ontology,
        ];

        if (sizeof($where)!=0) {
            $where = "({$where}) AND ";
        }

        $where .= "(".$this->filter_ontology($ontologies).")";

        $where .= " AND ({Team::$alias}.narrow='{$this->narrow()}')";

        $return = array_keys($ontologies);

        $qs = $this->query("MATCH ${match} WHERE $where RETURN {$return}", $args);

        return $qs;
    }
}

