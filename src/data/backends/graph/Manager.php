<?php

namespace PHP2USE\data\backends\graph;

/****************************************************************************/

class Manager extends \PHP2USE\data\Provider {
    private static $graphs = [];

    /***********************************************************************/

    public static function instances () {
        return array_keys(Manager::$graphs);
    }

    public static function instance ($key=null) {
        if ($key==null) {
            $key = 'default';
        }

        if (array_key_exists($key, Manager::$graphs)) {
            return Manager::$graphs[$key];
        } else {
            return null;
        }
    }

    /***********************************************************************/

    private static function reflection ($func, $raw) {
        $name = $raw[0]; array_shift($raw);
        
        return call_user_func_array([Manager::instance($name),$func], $raw);
    }

    public static function query    () { return Manager::reflection('query', func_get_args()); }
    public static function traverse () { return Manager::reflection('traverse', func_get_args()); }

    /***********************************************************************/

    public static function initialize () {
        if (!array_key_exists('default', Manager::$graphs)) {
            Manager::$graphs['default'] = new Graph(APP_GRAPH);
        }
    }

    /***********************************************************************/

    public static function before () {
        Manager::$graphs['default'] = new NeoGraph(APP_GRAPH);
    }

    /***********************************************************************/

    public static function after () {
        
    }
}

