<?php

namespace PHP2USE\data\backends\graph;

use PHP2USE\web\Application;

use Neoxygen\NeoClient\ClientBuilder;

/****************************************************************************/

class Graph extends \PHP2USE\data\Dimension { # A Neo4j graph
    private $cnx;
    
    /***********************************************************************/

    public function __construct ($uri) {
        $this->url = parse_url($uri);

        parent::__construct([]);

        $this->cnx = ClientBuilder::create()
            ->addConnection('default', $this->url['scheme'], $this->url['host'], $this->url['port'], true, $this->url['user'], $this->url['pass'])
            ->setLogger('app', Application::curr()->log())
            ->setAutoFormatResponse(true)
            ->setDefaultTimeout(20)
            ->build();
    }

    /***********************************************************************/

    public function cursor () { return $this->cnx; }
    
    /***********************************************************************/

    public function query ($stmt, $params=[]) {
        return new Traversal($this, $stmt, $params);
    }

    public function traverse ($match="n", $where="", $ontologies=['n'=>[]]) {
        $args = [];

        if (strlen($where)!=0) {
            $where = "({$where}) AND ";
        }

        $where .= "(".$this->filter_ontology($ontologies).")";

        $return = implode(', ', array_keys($ontologies));

        $qs = $this->query("MATCH ${match} WHERE $where RETURN {$return}", $args);

        return $qs;
    }
    
    private function filter_ontology ($mapping=[]) {
        $resp = [];

        foreach ($mapping as $nrw => $lst) {
            foreach ($lst as $key) {
                $resp[] = "{$nrw}.ontology='{$key}'";
            }
        }

        if (sizeof($resp)) {
            return implode(' or ', $resp);
        } else {
            return "True";
        }
    }
    
    /***********************************************************************/

    public function load_fs ($root, $recursive=false) {
        foreach ([] as $sub) {
            
        }
    }
}

