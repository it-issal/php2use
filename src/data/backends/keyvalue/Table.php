<?php

namespace PHP2USE\data\keyvalue;

/****************************************************************************/

class Table extends \PHP2USE\core\hive\Namespace {
    private static $graph;
    
    /***********************************************************************/

    public function __construct ($uri) {
        parent::__construct();

        $this->url = parse_url($uri);

        $this->cnx = ClientBuilder::create()
            ->addConnection('default', $this->url['scheme'], $this->url['host'], $this->url['port'], true, $this->url['user'], $this->url['pass'])
            ->setLogger('app', ORM::log())
            ->setAutoFormatResponse(true)
            ->setDefaultTimeout(20)
            ->build();
    }

    /***********************************************************************/

    public function cursor () { return $this->cnx; }
    
    /***********************************************************************/

    public function query ($stmt, $params=[]) {
        return new NeoResult($graph, $query, $params, $payload);
    }
    
    /***********************************************************************/

    public function load_fs ($path, $recursive=false) {
        
    }
}

