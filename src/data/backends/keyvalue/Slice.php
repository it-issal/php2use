<?php

namespace PHP2USE\data\keyvalue;

/****************************************************************************/

class Slice extends \PHP2USE\core\hive\Lookup {
    private $grp;

    public function __construct ($graph, $statement, $params) {
        parent::__construct();

        $this->grp  = $graph;

        $this->stmt = $statement;
        $this->args = $params;

        $this->nodes = [];
        $this->edges = [];

        $this->reset();
    }

    public function graph     () { return $this->grp; }
    public function statement () { return $this->stmt; }
    public function args      () { return $this->args; }

    public function query     () { return $this->query; }
    public function payload   () { return $this->payload; }

    public function reset () {
        $this->query   = $this->cursor()->sendCypherQuery($this->stmt, $this->args);

        $result = $this->query->getResult();

        $this->nodes = [];
        $this->edges = [];

        $positions = [];
        $i = 0;

        foreach ($result->getNodes() as $node){
            $prop = ($node->getLabel() === 'Movie') ? 'title' : 'name';
            $this->nodes[] = [
                'title' => $node->getProperty($prop),
                'label' => $node->getLabel()
            ];
            $positions[$node->getId()] = $i;
            $i++;
        }

        foreach ($result->getRelationships() as $rel){
            $this->edges[] = [
                'source' => $positions[$rel->getStartNode()->getId()],
                'target' => $positions[$rel->getEndNode()->getId()]
            ];
        }

        $this->nodes = [];
        $this->edges = [];

        foreach ($result->getNodes() as $node){
            $prop = ($node->getLabel() === 'Movie') ? 'title' : 'name';

            $this->nodes[$node->getId()] = [
                'title' => $node->getProperty($prop),
                'label' => $node->getLabel()
            ];
        }

        foreach ($result->getRelationships() as $rel){
            $this->edges[] = [
                'source' => $this->nodes[$rel->getStartNode()->getId()],
                'target' => $this->nodes[$rel->getEndNode()->getId()]
            ];
        }
    }

    public function nodes     () { return $this->nodes; }
    public function edges     () { return $this->edges; }
}

