<?php

namespace PHP2USE\data\keyvalue;

/****************************************************************************/

class Manager extends \PHP2USE\core\hive\Provider {
    private static $graphs = [];

    /***********************************************************************/

    public static function graphs () {
        return array_keys(ORM::$graphs);
    }

    public static function graph ($key='default') {
        if (array_key_exists(ORM::$graphs)) {
            return ORM::$graphs[$key];
        } else {
            return null;
        }
    }
    
    /***********************************************************************/

    protected function before () {
        $this->cfg['default'] = [
            'connection'    => APP_MONGO,
            //'connection'    => "mongodb://{$cnx['host']}:{$cnx['port']}",
            //'username'      => $cnx['user'],
            //'password'      => $cnx['pass'],
            'db'            => substr($cnx['path'], 1),
        ];
    }

    /***********************************************************************/

    protected function after () {
        
    }
}

