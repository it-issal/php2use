<?php

namespace PHP2USE\data;

/***************************************************************************/

class Provider extends Primitive {
    public static function ns      () { return Reactor::ns(); }

    /***********************************************************************/

    public static function app     () { return Reactor::app(); }
    public static function cfg     () { return Reactor::cfg(); }
    public static function log     () { return Reactor::log(); }
}

