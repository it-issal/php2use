<?php

if ( ! file_exists($file = __DIR__.'/vendor/autoload.php')) {
    throw new RuntimeException('Install dependencies to run this script.');
}

/****************************************************************************/

define('APP_ROOT',  __DIR__);
define('APP_PATH',  APP_ROOT.'/app');
define('APP_CACHE', APP_ROOT.'/cache');
define('APP_DATA',  APP_ROOT.'/data');

define('APP_LIB',   APP_ROOT.'/lib');
define('APP_SRC',   APP_ROOT.'/src');

/****************************************************************************/

$defaults = [
    'graph' => "http://scuba_dev:zdZ4ejSxxgMMtRdSwhcT@scubadev.sb05.stations.graphenedb.com:24789/",
    'mongo' => "",
];

$conns = [];

foreach ([
    ['mongo', 'cluster', ['scuba']],  //, 'local']],
    ['graph', 'graph',   ['vortex']], //, 'local']],
] as $cfg) {
    foreach ($cfg[2] as $key) {
        $x = getenv(strtoupper($cfg[1]).'_'.strtoupper($key));

        if ($x!=null and strlen($x)!=0) {
            $conns[$cfg[0]] = $x;
        }
    }
}

foreach ($defaults as $key => $value) {
    if (!array_key_exists($key, $conns)) {
        $conns[$key] = $value;
    }

    define("APP_".strtoupper($key), $conns[$key]);
}

/****************************************************************************/

$loader = require_once $file;

$loader->addPsr4('Schemas\\', [APP_PATH.'/schemas']);
$loader->addPsr4('Silos\\',   [APP_PATH.'/silos']);

$loader->addPsr4('PHP2USE\\', [APP_SRC.'/php2use/src']);

/****************************************************************************/

global $argv, $argc, $argi;

$defaults = [
    'argv' => [],
    'argc' => 0,
    'argi' => 0,
];

foreach (array_keys($defaults) as $key) {
    if ($$key==null) {
        $$key = $defaults[$key];
    }
}

use \PHP2USE\web\Application;

$argi = 0;

while (($argi < $argc) and in_array($argv[$argi], [
    '/usr/bin/php5',
    '/usr/bin/php',
    'php5',
    'php',
    'index.php',
])) {
    $argi += 1;
}

$cmd = 'web';

if ($argi < $argc) {
    $cmd = $argv[$argi];
}

switch ($cmd) {
    case 'console':
        //Application::bootstrap(null, 'console');
        break;
    case 'web':
        if (array_key_exists('HTTP_HOST', $_SERVER)) {
            $target = null;

            if (array_key_exists('REQUEST_URI', $_SERVER)) {
                if ($_SERVER['REQUEST_URI']!='/') {
                    if ($_SERVER['SCRIPT_FILENAME']!="{$_SERVER['DOCUMENT_ROOT']}/index.php") {
                        $target = $_SERVER['SCRIPT_FILENAME'];
                    }
                }
            }

            if (file_exists($target)==true) {
                return false;
            } else {
                Application::bootstrap($_SERVER['HTTP_HOST'], 'slim');
            }
        }
        break;
    case 'worker':
        Application::bootstrap(null, 'gearman');
        break;
    case 'help':
        echo "Command unknown : ".var_dump($argv)." .\n";
        echo "Try :\n";
        echo "\t*) console : Interactive commandline.\n";
        echo "\t*) web     : Starts the web server.\n";
        echo "\t*) worker  : Lunch a new task worker.\n";
        break;
}

/****************************************************************************/

use PHP2USE\data\backends\graph\Manager as GraphDB;

GraphDB::initialize();

/************************************************************************/

Application::curr()->run();

