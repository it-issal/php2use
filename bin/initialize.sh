#!/bin/bash

export TARGET_DIR=$1
export TARGET_LIB=$1/$2

cp -afR $TARGET_LIB/usr/* $TARGET_DIR

cd $TARGET_DIR

mkdir -p $TARGET_DIR/{app,cache/{config,doctrine,logs,templates},themes}

if [[ ! -d vendor ]] ; then
    composer install

    composer update
fi
